﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class Login_Medico : Form
    {

        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        private string strSql1 = string.Empty;
        public Login_Medico()
        {
            InitializeComponent();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            HistoricoMedicoAgenda m = new HistoricoMedicoAgenda();
            m.Show();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Close();
            RelatorioMedico r = new RelatorioMedico();
            r.Show();
        }

        private void btPesquisarPaciente_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();

                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Paciente where nome like'" + txtPesquisarPaciente.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);


                dataGridView3.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                connection conexao = new connection();


                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Agendamento_consulta where Data like'" + txtPesquisar.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();



                instrucao.Fill(tabela);




                dataGridView1.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtCodigoConsulta.Text == string.Empty)
            {
                MessageBox.Show("por favor digitar o codigo do agendamento para verificar a consulta");
            }
            else
            {


                strSql1 = "SELECT * FROM Agendamento_consulta WHERE cod_consulta = @cod_consulta";
                Sqlcon = new SqlConnection(Strcon);
                SqlCommand cmd1 = new SqlCommand(strSql1, Sqlcon);
                cmd1.Parameters.Add("@cod_consulta", SqlDbType.VarChar).Value = txtCodigoConsulta.Text;


                try
                {


                    if (txtCodigoConsulta.Text == String.Empty)
                    {
                        throw new Exception("você precisa digitar o codigo da consulta!!!");
                    }
                    Sqlcon.Open();
                    SqlDataReader reader = cmd1.ExecuteReader();

                    if (reader.HasRows == false)
                    {

                        throw new Exception("consulta cadastrada com sucesso!!!");

                    }

                    reader.Read();


                    txtCodigoConsulta.Text = Convert.ToString(reader["cod_consulta"]);

                    txtStatusAgendamento.Text = Convert.ToString(reader["Status_Consulta"]);



                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.ToString());
                }
                finally
                {
                    Sqlcon.Close();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtCodigoConsulta.Text == string.Empty)
            {
                MessageBox.Show("por favor digitar o codigo do agendamento para alterar a consulta");
            }
            else
            {

                AlterarConsultaMedico alter2 = new AlterarConsultaMedico(int.Parse(txtCodigoConsulta.Text), txtStatusAgendamento.Text);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            prontuarioMedico m = new prontuarioMedico();
            m.Show();
        }

        private void Login_Medico_Load(object sender, EventArgs e)
        {

        }
    }
}
