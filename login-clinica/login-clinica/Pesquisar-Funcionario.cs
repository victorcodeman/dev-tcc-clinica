﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class Pesquisar_Funcionario : Form
    {
        public Pesquisar_Funcionario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(TipoPesquisa.SelectedItem == "Codigo")
            {
                PesquisaFuncionario pe = new PesquisaFuncionario(txtPesquisaFuncionario.Text,dataGridView1);
            }
         
            else if (TipoPesquisa.SelectedItem == "Especialidade")
            {
                PesquisaFuncionario pe = new PesquisaFuncionario(txtPesquisaFuncionario.Text, dataGridView1);
                pe.PesquisarEspecialidade(txtPesquisaFuncionario.Text, dataGridView1);
            }
            else if (TipoPesquisa.SelectedItem == "Cargo")
            {
                PesquisaFuncionario pe3 = new PesquisaFuncionario(txtPesquisaFuncionario.Text, dataGridView1);
                pe3.PesquisarCargo(txtPesquisaFuncionario.Text, dataGridView1);
            }

            else
            {
                PesquisaFuncionario pe = new PesquisaFuncionario(txtPesquisaFuncionario.Text, dataGridView1);
                pe.PesquisarNome(txtPesquisaFuncionario.Text, dataGridView1);
            }


        }






        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void entrarLoginDeAdminstradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 f1 = new Form3();
            f1.Show();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico m1 = new Cadastro_medico();
            m1.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario f1 = new Cadastro_funcionario();
            f1.Show();
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente p1 = new Pesquisar_Paciente();
            p1.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico m1 = new pesquisar_Medico();
            m1.Show();
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 p = new Form2();
            p.Show();
        }

        private void especalidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade pes = new pesquisaMedicoEspecialidade();
            pes.Show();
        }

        private void btSair_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }
    }
}
