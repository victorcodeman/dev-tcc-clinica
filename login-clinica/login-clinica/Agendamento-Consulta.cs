﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class Agendamento_Consulta : Form
    {
        public event System.Windows.Forms.TypeValidationEventHandler TypeValidationCompleted;
        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        private string strSql1 = string.Empty;
        public Agendamento_Consulta()
        {
            InitializeComponent();
        }

        private void btImagem_Click(object sender, EventArgs e)
        {
     

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void Agendamento_Consulta_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
               
        connection conexao = new connection();
              
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Agendamento_consulta where Data like'" + txtPesquisarMedico.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();
                
                instrucao.Fill(tabela);
      
                dataGridView1.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            
           

            if (txtCodigo1.Text == string.Empty || txtMedico.Text == string.Empty || txtTipoConsulta.Text == string.Empty || !txtData.MaskCompleted || !txtDataAgendamento.MaskCompleted || !txtDataConsulta.MaskCompleted || txtTipoAgendamento.Text == string.Empty || txtStatusAgendamento.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*)!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                String Data = txtDataConsulta.Text;
                CadConsulta cad1 = new CadConsulta(int.Parse(txtCodigoConsulta.Text),txtTipoAgendamento.Text, txtTipoConsulta.Text, Convert.ToDateTime(txtDataConsulta.Text), Convert.ToDateTime(txtData.Text), int.Parse(txtMedico.Text), int.Parse(txtCodigo1.Text), Convert.ToDateTime(txtDataAgendamento.Text), txtStatusAgendamento.Text, Data);
                Close();
                Agendamento_Consulta a = new Agendamento_Consulta();
                a.Show();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {

            if (txtCodigo1.Text == string.Empty || txtMedico.Text == string.Empty || txtTipoConsulta.Text == string.Empty || !txtData.MaskCompleted || !txtDataAgendamento.MaskCompleted || !txtDataConsulta.MaskCompleted || txtTipoAgendamento.Text == string.Empty || txtStatusAgendamento.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*) para alterar os dados do agendamento!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                String Data = txtDataConsulta.Text;
                AlterarConsulta alter2 = new AlterarConsulta(int.Parse(txtCodigoConsulta.Text), txtTipoAgendamento.Text, txtTipoConsulta.Text, DateTime.Parse(txtDataConsulta.Text), DateTime.Parse(txtData.Text), int.Parse(txtMedico.Text), int.Parse(txtCodigo1.Text), DateTime.Parse(txtDataAgendamento.Text), txtStatusAgendamento.Text, Data);
                Close();
                Agendamento_Consulta a = new Agendamento_Consulta();
                a.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtCodigo1.Text == string.Empty || txtMedico.Text == string.Empty || txtTipoConsulta.Text == string.Empty || !txtData.MaskCompleted || !txtDataAgendamento.MaskCompleted || !txtDataConsulta.MaskCompleted || txtTipoAgendamento.Text == string.Empty || txtStatusAgendamento.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*) para deletar os dados do agendamento!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                String Data = txtDataConsulta.Text;
                CadHistoricoAgendamento cad1 = new CadHistoricoAgendamento(int.Parse(txtCodigoConsulta.Text),txtTipoAgendamento.Text, txtTipoConsulta.Text, Convert.ToDateTime(txtDataConsulta.Text), Convert.ToDateTime(txtData.Text), int.Parse(txtMedico.Text), int.Parse(txtCodigo1.Text), Convert.ToDateTime(txtDataAgendamento.Text), txtStatusAgendamento.Text, Data);


                DeletarConsulta del1 = new DeletarConsulta(int.Parse(txtCodigoConsulta.Text), txtTipoAgendamento.Text, txtTipoConsulta.Text, DateTime.Parse(txtDataConsulta.Text), Convert.ToDateTime(txtData.Text), int.Parse(txtMedico.Text), int.Parse(txtCodigo1.Text), DateTime.Parse(txtDataAgendamento.Text), txtStatusAgendamento.Text, Data);
                Close();
                Agendamento_Consulta a = new Agendamento_Consulta();
                a.Show();
            }

        }
        private void button6_Click(object sender, EventArgs e)
        {
            if(txtCodigoConsulta.Text == string.Empty)
            {
                MessageBox.Show("por favor digite o codigo da consulta para verificar o agendamento");
            }



            strSql1 = "SELECT * FROM Agendamento_consulta WHERE cod_consulta = @cod_consulta";
            Sqlcon = new SqlConnection(Strcon);
            SqlCommand cmd1 = new SqlCommand(strSql1, Sqlcon);
            cmd1.Parameters.Add("@cod_consulta", SqlDbType.VarChar).Value = txtCodigoConsulta.Text;


            try
            {


                if (txtCodigoConsulta.Text == String.Empty)
                {
                    throw new Exception("você precisa digitar o codigo da consulta!!!");
                }
                Sqlcon.Open();
                SqlDataReader reader = cmd1.ExecuteReader();

                if (reader.HasRows == false)
                {

                    throw new Exception("consulta cadastrada com sucesso!");

                }

                reader.Read();


                txtCodigoConsulta.Text = Convert.ToString(reader["cod_consulta"]);
                txtDataConsulta.Text = Convert.ToString(reader["Data_Consulta"]);
                txtMedico.Text = Convert.ToString(reader["Medico"]);
                txtCodigo1.Text = Convert.ToString(reader["Cod_pacient"]);
                txtTipoConsulta.Text = Convert.ToString(reader["Tipo_Consulta"]);
                txtDataAgendamento.Text = Convert.ToString(reader["Data_Agendamento"]);
                txtTipoAgendamento.Text = Convert.ToString(reader["Tipo_Agendamento"]);
                txtStatusAgendamento.Text = Convert.ToString(reader["Status_Consulta"]);
                txtData.Text = Convert.ToString(reader["DataAte"]);



            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                Sqlcon.Close();
            }
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente p1 = new Pesquisar_Paciente();
            p1.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico m1 = new pesquisar_Medico();
            m1.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 c1 = new Form3();
            c1.Show();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico m1 = new Cadastro_medico();
            m1.Show();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void pesquisarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void especialidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade m1 = new pesquisaMedicoEspecialidade();
            m1.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario f1 = new Cadastro_funcionario();
            f1.Show();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void principalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 p = new Form2();
            p.Show();
        }

        private void entrarComoAdiministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            try
            {
                connection conexao = new connection();

              
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Medico where nome_medico like'" + txtListaMedico.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();



             

                instrucao.Fill(tabela);
        


                dataGridView2.DataSource = tabela;

          


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btPesquisarPaciente_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();

                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Paciente where nome like'" + txtPesquisarPaciente.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);
                

                dataGridView3.DataSource = tabela;

                dataGridView3.Columns[0].HeaderText = "Identificação ";
                dataGridView3.Columns[1].HeaderText = "Nome ";
                dataGridView3.Columns[2].HeaderText = "Data de Nascimento ";
                dataGridView3.Columns[3].HeaderText = "Sexo ";
                dataGridView3.Columns[4].HeaderText = "Tipo Sanguinio ";
                dataGridView3.Columns[5].HeaderText = "Endereço ";
                dataGridView3.Columns[6].HeaderText = "Bairro ";
                dataGridView3.Columns[7].HeaderText = "Cidade ";
                dataGridView3.Columns[8].HeaderText = "Estado ";
                dataGridView3.Columns[9].HeaderText = "CEP ";
                dataGridView3.Columns[10].HeaderText = "Telefone ";
                dataGridView3.Columns[11].HeaderText = "Celular ";
                dataGridView3.Columns[12].HeaderText = "RG ";
                dataGridView3.Columns[13].HeaderText = "CPF ";
                dataGridView3.Columns[14].HeaderText = "Estado Civil ";
                dataGridView3.Columns[15].HeaderText = "Profissão ";
                dataGridView3.Columns[16].HeaderText = "Email ";
                dataGridView3.Columns[17].HeaderText = "Naturalidade ";
                dataGridView3.Columns[18].HeaderText = "Cor ";
                dataGridView3.Columns[19].HeaderText = "Grau de Instrução ";
                dataGridView3.Columns[20].HeaderText = "Conjuge ";
                dataGridView3.Columns[21].HeaderText = "Mãe ";
                dataGridView3.Columns[22].HeaderText = "Pai ";


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void txtDataConsulta_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
          
        }

        private void txtDataConsulta_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtDataConsulta.Clear();
            }
        }

        private void txtData_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtData.Clear();
            }
        }

        private void txtDataAgendamento_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtDataAgendamento.Clear();
            }
        }

        private void txtDataConsulta_MaskInputRejected_1(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }
    }
    }
    
    

