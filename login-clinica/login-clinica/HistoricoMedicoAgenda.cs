﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class HistoricoMedicoAgenda : Form
    {
        public HistoricoMedicoAgenda()
        {
            InitializeComponent();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Close();
            RelatorioMedico r = new RelatorioMedico();
            r.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            prontuarioMedico m = new prontuarioMedico();
            m.Show();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                connection conexao = new connection();


                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Historico_Agendamento where Data like'" + txtPesquisarMedico.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();



                instrucao.Fill(tabela);




                dataGridView1.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            HistoricoMedicoAgenda m = new HistoricoMedicoAgenda();
            m.Show();
        }

        private void btPesquisarPaciente_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();

                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Paciente where nome like'" + txtPesquisarPaciente.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);


                dataGridView3.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
            Login_Medico m = new Login_Medico();
            m.Show();
        }
    }
}
