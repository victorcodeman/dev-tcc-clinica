﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class Form3 : Form
    {
        Bitmap bmp;

        const string string_conexao = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";

        SqlConnection conexao = new SqlConnection(string_conexao);

        public Form3()
        {
            InitializeComponent();
        }
   

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (pictureBox1.Image == null)
            {

                MessageBox.Show("Não pode efetuar o cadastro sem Imagem", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            else if (txtNome.TextLength < 5 || txtCodigo.Text == string.Empty || !txtCpf.MaskCompleted || !txtNascimento.MaskCompleted || !txtRg.MaskCompleted || !txtTelefone.MaskCompleted || !txtCep.MaskCompleted || !txtCelular.MaskCompleted || txtEndereco.TextLength < 4 || txtCidade.TextLength < 3 || txtBairro.TextLength < 4 || txtEstado.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*)!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            
            else{
                Regex regex = new Regex(@"[^\d]");
                CadPaciente paciente1 = new CadPaciente(int.Parse(txtCodigo.Text), txtNome.Text, Convert.ToDateTime(txtNascimento.Text), txtSexo.Text, txtTipoSanguineo.Text, txtEndereco.Text, txtBairro.Text, txtCidade.Text, txtEstado.Text, Convert.ToInt64(regex.Replace(txtCep.Text, "")), Convert.ToInt64(regex.Replace(txtTelefone.Text, "")), Convert.ToInt64(regex.Replace(txtCelular.Text, "")), Convert.ToInt64(regex.Replace(txtRg.Text, "")), Convert.ToInt64(regex.Replace(txtCpf.Text, "")), txtEstadoCivil.Text, txtProfissao.Text, txtEmail.Text, txtNaturalidade.Text, txtCor.Text, txtGrauDeInstrucao.Text, txtConjuge.Text, txtMae.Text, txtPai.Text);

                MemoryStream memory = new MemoryStream();

                bmp.Save(memory, ImageFormat.Bmp);

                byte[] foto = memory.ToArray();

                const string string_conexao = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
               

                SqlConnection dbConexao = new SqlConnection(string_conexao);

                SqlCommand cmd = new SqlCommand("insert into ImagemPaciente (nome, foto) values (@nome,@foto)", dbConexao);

                SqlParameter nome = new SqlParameter("@nome", SqlDbType.VarChar);
                SqlParameter imagem = new SqlParameter(@"foto", SqlDbType.VarBinary);

                nome.Value = txtCodigo.Text;

                imagem.Value = foto;

                cmd.Parameters.Add(nome);

                cmd.Parameters.Add(imagem);


                try
                {

                    dbConexao.Open();

                    cmd.ExecuteNonQuery();

                    txtCodigo.Text = " ";

                    pictureBox1.Image = null;

                }
                catch (Exception E)
                {

                    MessageBox.Show(E.Message);

                }
                finally
                {

                    dbConexao.Close();
                    Close();
                    Form3 pr = new Form3();
                    pr.Show();

                }



            }


        }

        private void Form3_Load(object sender, EventArgs e)
        {
             
        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void txtRg_TextChanged(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void txtCpf_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void txtEstadoCivil_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMae_TextChanged(object sender, EventArgs e)
        {
            txtMae.MaxLength = 50;
        }

        private void btImagem_Click(object sender, EventArgs e)
        {
           
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                string nome = openFileDialog1.FileName;

                bmp = new Bitmap(nome);

                pictureBox1.Image = bmp;


            }
        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {
            txtNome.MaxLength = 50;
        }

        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtNascimento_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtSexo_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txtTipoSanguineo_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtCep_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtEstado_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEndereco_TextChanged(object sender, EventArgs e)
        {
            txtEndereco.MaxLength = 50;
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {
            txtCidade.MaxLength = 30;
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void txtBairro_TextChanged(object sender, EventArgs e)
        {
            txtBairro.MaxLength = 30;
        }

        private void Endereço_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void txtTelefone_TextChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void txtCelular_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void txtTelefone2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtImg_TextChanged(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void txtConvenio_TextChanged(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void txtCodConvenio_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            txtEmail.MaxLength = 50;
        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void txtGrauDeInstrucao_TextChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void txtCor_TextChanged(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void txtNaturalidade_TextChanged(object sender, EventArgs e)
        {
            txtNaturalidade.MaxLength = 30;
        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void txtProfissao_TextChanged(object sender, EventArgs e)
        {
            txtProfissao.MaxLength = 30;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void txtConjuge_TextChanged(object sender, EventArgs e)
        {
            txtConjuge.MaxLength = 50;
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void Pai_Click(object sender, EventArgs e)
        {

        }

        private void txtPai_TextChanged(object sender, EventArgs e)
        {
            txtPai.MaxLength = 50;
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente p1 = new Pesquisar_Paciente();
            p1.Show();
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
                principal.Show();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico medico = new Cadastro_medico();
            medico.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico medico = new pesquisar_Medico();
            medico.Show();
        }

        private void especalidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade especialidade = new pesquisaMedicoEspecialidade();
            especialidade.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;

        private void button6_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text == string.Empty)
            {
                MessageBox.Show("por favor digitar o codigo do paciente para verificar seus dados");
            }
            else
            {

                try
                {

                    conexao.Open();
                    SqlCommand cmd2 = new SqlCommand("select LTRIM (nome),foto from ImagemPaciente where LTRIM(nome)=@nome ", conexao);

                    SqlParameter nomes1 = new SqlParameter("@nome", SqlDbType.VarChar);

                    nomes1.Value = txtCodigo.Text;

                    cmd2.Parameters.Add(nomes1);

                    SqlDataReader reader = cmd2.ExecuteReader();


                    Byte[] imagem = new Byte[1];
                    if (reader.Read())
                    {
                        txtCodigo.Text = reader[0].ToString();

                        imagem = (Byte[])(reader[1]);

                        if (imagem == null)
                        {

                            pictureBox1.Image = null;

                        }
                        else
                        {
                            MemoryStream memory = new MemoryStream(imagem);

                            pictureBox1.Image = Image.FromStream(memory);
                            pictureBox1.Refresh();


                        }

                        pictureBox1.Refresh();
                    }
                    conexao.Close();

                }


                catch (Exception E)
                {

                    MessageBox.Show(E.Message);
                }

                SqlCommand comando = new SqlCommand("select * from Cadastro_Paciente where nome = @nomes ", conexao);


                SqlParameter nomes = new SqlParameter("@nomes", SqlDbType.VarChar);

                nomes.Value = txtCodigo.Text;

                comando.Parameters.Add(nomes);



                try
                {

                    conexao.Open();

                    SqlDataReader dr = comando.ExecuteReader();

                    dr.Read();

                    if (dr.HasRows)
                    {

                        txtCodigo.Text = dr[0].ToString();
                        byte[] imagem = (byte[])(dr[1]);

                        if (imagem == null)
                            pictureBox1.Image = null;
                        else
                        {
                            MemoryStream memory = new MemoryStream(imagem);


                            pictureBox1.Image = Image.FromStream(memory);

                        }

                    }
                    conexao.Close();

                }
                catch (Exception E)
                {

                    MessageBox.Show(E.Message);
                }


                strSql = "SELECT * FROM Cadastro_Paciente WHERE Cod_Paciente = @Cod_Paciente";
                Sqlcon = new SqlConnection(Strcon);
                SqlCommand cmd = new SqlCommand(strSql, Sqlcon);
                cmd.Parameters.Add("@Cod_paciente", SqlDbType.VarChar).Value = txtCodigo.Text;


                try
                {


                    if (txtCodigo.Text == String.Empty)
                    {
                        throw new Exception("você precisa digitar o codigo do paciente!!!");
                    }
                    Sqlcon.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows == false)
                    {

                        throw new Exception("Paciente cadastrado com sucesso!");

                    }

                    reader.Read();


                    txtCodigo.Text = Convert.ToString(reader["Cod_paciente"]);
                    txtNome.Text = Convert.ToString(reader["nome"]);
                    txtEndereco.Text = Convert.ToString(reader["endereco"]);
                    txtCep.Text = Convert.ToString(reader["cep"]);
                    txtBairro.Text = Convert.ToString(reader["bairro"]);
                    txtCidade.Text = Convert.ToString(reader["cidade"]);
                    txtCelular.Text = Convert.ToString(reader["celular"]);
                    txtTelefone.Text = Convert.ToString(reader["telefone"]);

                    txtRg.Text = Convert.ToString(reader["rg"]);
                    txtSexo.Text = Convert.ToString(reader["sexo"]);
                    txtPai.Text = Convert.ToString(reader["pai"]);
                    txtMae.Text = Convert.ToString(reader["mae"]);
                    txtNascimento.Text = Convert.ToString(reader["Nascimento"]);
                    txtTipoSanguineo.Text = Convert.ToString(reader["Tipo_Sanguinio"]);
                    txtEstado.Text = Convert.ToString(reader["estado"]);
                    txtNaturalidade.Text = Convert.ToString(reader["naturalidade"]);
                    txtCor.Text = Convert.ToString(reader["cor"]);
                    txtEstadoCivil.Text = Convert.ToString(reader["estado_civil"]);
                    txtConjuge.Text = Convert.ToString(reader["conjuge"]);
                    txtEmail.Text = Convert.ToString(reader["email"]);
                    txtGrauDeInstrucao.Text = Convert.ToString(reader["Grau_de_instrucao"]);
                    txtCpf.Text = Convert.ToString(reader["cpf"]);
                    txtProfissao.Text = Convert.ToString(reader["profissao"]);



                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.ToString());
                }
                finally
                {
                    Sqlcon.Close();
                }

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (txtNome.TextLength < 5 || txtCodigo.Text == string.Empty || !txtCpf.MaskCompleted || !txtNascimento.MaskCompleted || !txtRg.MaskCompleted || !txtTelefone.MaskCompleted || !txtCep.MaskCompleted || !txtCelular.MaskCompleted || txtEndereco.TextLength < 4 || txtCidade.TextLength < 3 || txtBairro.TextLength < 4 || txtEstado.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios para alterar o cadastro (*)!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else 
            {

                Regex regex = new Regex(@"[^\d]");
                AlterarPaciente teste = new AlterarPaciente(int.Parse(txtCodigo.Text), txtNome.Text, Convert.ToDateTime(txtNascimento.Text), txtSexo.Text, txtTipoSanguineo.Text, txtEndereco.Text, txtBairro.Text, txtCidade.Text, txtEstado.Text, Convert.ToInt64(regex.Replace(txtCep.Text, "")), Convert.ToInt64(regex.Replace(txtTelefone.Text, "")), Convert.ToInt64(regex.Replace(txtCelular.Text, "")), Convert.ToInt64(regex.Replace(txtRg.Text, "")), Convert.ToInt64(regex.Replace(txtCpf.Text, "")), txtEstadoCivil.Text, txtProfissao.Text, txtEmail.Text, txtNaturalidade.Text, txtCor.Text, txtGrauDeInstrucao.Text, txtConjuge.Text, txtMae.Text, txtPai.Text);



                Close();
                Form3 pr = new Form3();
                pr.Show();






            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtNome.TextLength < 5 || txtCodigo.Text == string.Empty || !txtCpf.MaskCompleted || !txtNascimento.MaskCompleted || !txtRg.MaskCompleted || !txtTelefone.MaskCompleted || !txtCep.MaskCompleted || !txtCelular.MaskCompleted || txtEndereco.TextLength < 4 || txtCidade.TextLength < 3 || txtBairro.TextLength < 4 || txtEstado.Text == string.Empty)
            {
             
                MessageBox.Show("por favor digitar o codigo para deletar o cadastro");
            }

            else
            {
                Regex regex = new Regex(@"[^\d]");
                DeletarPaciente delete = new DeletarPaciente(int.Parse(txtCodigo.Text), txtNome.Text, Convert.ToDateTime(txtNascimento.Text), txtSexo.Text, txtTipoSanguineo.Text, txtEndereco.Text, txtBairro.Text, txtCidade.Text, txtEstado.Text, Convert.ToInt64(regex.Replace(txtCep.Text, "")), Convert.ToInt64(regex.Replace(txtTelefone.Text, "")), Convert.ToInt64(regex.Replace(txtCelular.Text, "")), Convert.ToInt64(regex.Replace(txtRg.Text, "")), Convert.ToInt64(regex.Replace(txtCpf.Text, "")), txtEstadoCivil.Text, txtProfissao.Text, txtEmail.Text, txtNaturalidade.Text, txtCor.Text, txtGrauDeInstrucao.Text, txtConjuge.Text, txtMae.Text, txtPai.Text);


                MemoryStream memory = new MemoryStream();

                byte[] foto = memory.ToArray();

                const string string_conexao = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";

                SqlConnection dbConexao = new SqlConnection(string_conexao);


                SqlCommand cmd = new SqlCommand("Delete from ImagemPaciente where nome = @nome", dbConexao);

                SqlParameter nome = new SqlParameter("@nome", SqlDbType.VarChar);
                SqlParameter imagem = new SqlParameter(@"foto", SqlDbType.VarBinary);

                nome.Value = txtCodigo.Text;

                imagem.Value = foto;

                cmd.Parameters.Add(nome);

                cmd.Parameters.Add(imagem);


                try
                {

                    dbConexao.Open();

                    cmd.ExecuteNonQuery();

                    txtCodigo.Text = " ";

                    pictureBox1.Image = null;

                }
                catch (Exception E)
                {

                    MessageBox.Show(E.Message);

                }
                finally
                {

                    dbConexao.Close();
                    Close();
                    Form3 pr = new Form3();
                    pr.Show();

                }
            }

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
           



        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario f1 = new Cadastro_funcionario();
            f1.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 Pacinete = new Form3();
            Pacinete.Show();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void entreComAdministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btVerificarConsultas_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(bmp, -180, -60);
        }
      
        private void btImprimir_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            bmp = new Bitmap(this.Size.Width, this.Size.Height, g);
            Graphics mg = Graphics.FromImage(bmp);
            mg.CopyFromScreen(this.Location.X, this.Location.Y, -180, -60, this.Size);
            printPreviewDialog1.ShowDialog();
        }

        private void txtNascimento_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtNascimento.Clear();
            }
        }

        private void txtCodigo_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            
        }
    }
    }


