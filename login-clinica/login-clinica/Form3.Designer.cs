﻿namespace login_clinica
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.btImagem = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Endereço = new System.Windows.Forms.Label();
            this.txtConjuge = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMae = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPai = new System.Windows.Forms.TextBox();
            this.Pai = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtNaturalidade = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtProfissao = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btSair = new System.Windows.Forms.Button();
            this.btRelatorio = new System.Windows.Forms.Button();
            this.btAdm = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.principalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.telaPrincipalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesquisarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacienteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.medicoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.médicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.especialidadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administradorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entreComAdministradorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agendamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btSair1 = new System.Windows.Forms.Button();
            this.btVerificarConsultas = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.txtSexo = new System.Windows.Forms.ComboBox();
            this.txtEstadoCivil = new System.Windows.Forms.ComboBox();
            this.txtGrauDeInstrucao = new System.Windows.Forms.ComboBox();
            this.txtCor = new System.Windows.Forms.ComboBox();
            this.txtTipoSanguineo = new System.Windows.Forms.ComboBox();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.txtCpf = new System.Windows.Forms.MaskedTextBox();
            this.txtRg = new System.Windows.Forms.MaskedTextBox();
            this.txtNascimento = new System.Windows.Forms.MaskedTextBox();
            this.txtEstado = new System.Windows.Forms.ComboBox();
            this.txtCodigo = new System.Windows.Forms.MaskedTextBox();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1059, 470);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 91);
            this.button1.TabIndex = 0;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(352, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "* Nome:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(829, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "* Codigo:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(446, 197);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(363, 20);
            this.txtNome.TabIndex = 3;
            this.txtNome.TextChanged += new System.EventHandler(this.txtNome_TextChanged);
            // 
            // btImagem
            // 
            this.btImagem.Location = new System.Drawing.Point(944, 319);
            this.btImagem.Name = "btImagem";
            this.btImagem.Size = new System.Drawing.Size(160, 23);
            this.btImagem.TabIndex = 6;
            this.btImagem.Text = "Inserir imagem";
            this.btImagem.UseVisualStyleBackColor = true;
            this.btImagem.Click += new System.EventHandler(this.btImagem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(528, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Sexo:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(352, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "* Nascimento:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(634, 238);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Tipo Sanguineo:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(763, 277);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(166, 20);
            this.txtBairro.TabIndex = 16;
            this.txtBairro.TextChanged += new System.EventHandler(this.txtBairro_TextChanged);
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(446, 277);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(253, 20);
            this.txtEndereco.TabIndex = 15;
            this.txtEndereco.TextChanged += new System.EventHandler(this.txtEndereco_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(720, 280);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "* Bairro:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // Endereço
            // 
            this.Endereço.AutoSize = true;
            this.Endereço.Location = new System.Drawing.Point(352, 280);
            this.Endereço.Name = "Endereço";
            this.Endereço.Size = new System.Drawing.Size(63, 13);
            this.Endereço.TabIndex = 13;
            this.Endereço.Text = "* Endereço:";
            this.Endereço.Click += new System.EventHandler(this.Endereço_Click);
            // 
            // txtConjuge
            // 
            this.txtConjuge.Location = new System.Drawing.Point(440, 514);
            this.txtConjuge.Name = "txtConjuge";
            this.txtConjuge.Size = new System.Drawing.Size(477, 20);
            this.txtConjuge.TabIndex = 18;
            this.txtConjuge.TextChanged += new System.EventHandler(this.txtConjuge_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(357, 517);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Cônjuge:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtMae
            // 
            this.txtMae.Location = new System.Drawing.Point(440, 557);
            this.txtMae.Name = "txtMae";
            this.txtMae.Size = new System.Drawing.Size(477, 20);
            this.txtMae.TabIndex = 20;
            this.txtMae.TextChanged += new System.EventHandler(this.txtMae_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(362, 560);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Mãe:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtPai
            // 
            this.txtPai.Location = new System.Drawing.Point(444, 598);
            this.txtPai.Name = "txtPai";
            this.txtPai.Size = new System.Drawing.Size(473, 20);
            this.txtPai.TabIndex = 22;
            this.txtPai.TextChanged += new System.EventHandler(this.txtPai_TextChanged);
            // 
            // Pai
            // 
            this.Pai.AutoSize = true;
            this.Pai.Location = new System.Drawing.Point(362, 601);
            this.Pai.Name = "Pai";
            this.Pai.Size = new System.Drawing.Size(25, 13);
            this.Pai.TabIndex = 21;
            this.Pai.Text = "Pai:";
            this.Pai.Click += new System.EventHandler(this.Pai_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(815, 241);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "* CEP:";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(446, 314);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(116, 20);
            this.txtCidade.TabIndex = 25;
            this.txtCidade.TextChanged += new System.EventHandler(this.txtCidade_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(562, 318);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "* Estado:";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(357, 319);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "* Cidade:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(797, 322);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "* Celular:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(663, 320);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "* Telefone:";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(677, 422);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "* CPF:";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(563, 420);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "* RG:";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(817, 422);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 13);
            this.label15.TabIndex = 39;
            this.label15.Text = "Estado Civil:";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(794, 375);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 13);
            this.label18.TabIndex = 45;
            this.label18.Text = "Grau de instrução:";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // txtNaturalidade
            // 
            this.txtNaturalidade.Location = new System.Drawing.Point(446, 416);
            this.txtNaturalidade.Name = "txtNaturalidade";
            this.txtNaturalidade.Size = new System.Drawing.Size(116, 20);
            this.txtNaturalidade.TabIndex = 43;
            this.txtNaturalidade.TextChanged += new System.EventHandler(this.txtNaturalidade_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(997, 423);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(26, 13);
            this.label19.TabIndex = 42;
            this.label19.Text = "Cor:";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(357, 423);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "Naturalidade:";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(446, 368);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(344, 20);
            this.txtEmail.TabIndex = 52;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(357, 372);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 13);
            this.label21.TabIndex = 51;
            this.label21.Text = "E-mail:";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // txtProfissao
            // 
            this.txtProfissao.Location = new System.Drawing.Point(440, 468);
            this.txtProfissao.Name = "txtProfissao";
            this.txtProfissao.Size = new System.Drawing.Size(477, 20);
            this.txtProfissao.TabIndex = 54;
            this.txtProfissao.TextChanged += new System.EventHandler(this.txtProfissao_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(357, 468);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 13);
            this.label24.TabIndex = 53;
            this.label24.Text = "Profissão:";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(945, 171);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(159, 142);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 55;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btSair);
            this.panel5.Controls.Add(this.btRelatorio);
            this.panel5.Controls.Add(this.btAdm);
            this.panel5.Location = new System.Drawing.Point(0, 27);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1920, 58);
            this.panel5.TabIndex = 59;
            // 
            // btSair
            // 
            this.btSair.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btSair.BackgroundImage")));
            this.btSair.Location = new System.Drawing.Point(148, 3);
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(62, 52);
            this.btSair.TabIndex = 16;
            this.btSair.UseVisualStyleBackColor = true;
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // btRelatorio
            // 
            this.btRelatorio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btRelatorio.BackgroundImage")));
            this.btRelatorio.Location = new System.Drawing.Point(75, 2);
            this.btRelatorio.Name = "btRelatorio";
            this.btRelatorio.Size = new System.Drawing.Size(62, 52);
            this.btRelatorio.TabIndex = 10;
            this.btRelatorio.UseVisualStyleBackColor = true;
            this.btRelatorio.Click += new System.EventHandler(this.btRelatorio_Click);
            // 
            // btAdm
            // 
            this.btAdm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btAdm.BackgroundImage")));
            this.btAdm.Location = new System.Drawing.Point(5, 2);
            this.btAdm.Name = "btAdm";
            this.btAdm.Size = new System.Drawing.Size(62, 52);
            this.btAdm.TabIndex = 3;
            this.btAdm.UseVisualStyleBackColor = true;
            this.btAdm.Click += new System.EventHandler(this.btAdm_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.principalToolStripMenuItem,
            this.pesquisarToolStripMenuItem,
            this.cadastroToolStripMenuItem,
            this.médicoToolStripMenuItem,
            this.administradorToolStripMenuItem,
            this.agendamentoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1130, 24);
            this.menuStrip1.TabIndex = 58;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // principalToolStripMenuItem
            // 
            this.principalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.telaPrincipalToolStripMenuItem});
            this.principalToolStripMenuItem.Name = "principalToolStripMenuItem";
            this.principalToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.principalToolStripMenuItem.Text = "Inicial";
            // 
            // telaPrincipalToolStripMenuItem
            // 
            this.telaPrincipalToolStripMenuItem.Name = "telaPrincipalToolStripMenuItem";
            this.telaPrincipalToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.telaPrincipalToolStripMenuItem.Text = "Tela Principal";
            this.telaPrincipalToolStripMenuItem.Click += new System.EventHandler(this.telaPrincipalToolStripMenuItem_Click);
            // 
            // pesquisarToolStripMenuItem
            // 
            this.pesquisarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pacienteToolStripMenuItem,
            this.medicoToolStripMenuItem,
            this.funcionariosToolStripMenuItem});
            this.pesquisarToolStripMenuItem.Name = "pesquisarToolStripMenuItem";
            this.pesquisarToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.pesquisarToolStripMenuItem.Text = "Pesquisar";
            // 
            // pacienteToolStripMenuItem
            // 
            this.pacienteToolStripMenuItem.Name = "pacienteToolStripMenuItem";
            this.pacienteToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.pacienteToolStripMenuItem.Text = "Paciente";
            this.pacienteToolStripMenuItem.Click += new System.EventHandler(this.pacienteToolStripMenuItem_Click);
            // 
            // medicoToolStripMenuItem
            // 
            this.medicoToolStripMenuItem.Name = "medicoToolStripMenuItem";
            this.medicoToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.medicoToolStripMenuItem.Text = "Médico";
            this.medicoToolStripMenuItem.Click += new System.EventHandler(this.medicoToolStripMenuItem_Click);
            // 
            // funcionariosToolStripMenuItem
            // 
            this.funcionariosToolStripMenuItem.Name = "funcionariosToolStripMenuItem";
            this.funcionariosToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.funcionariosToolStripMenuItem.Text = "Funcionarios";
            this.funcionariosToolStripMenuItem.Click += new System.EventHandler(this.funcionariosToolStripMenuItem_Click);
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pacienteToolStripMenuItem1,
            this.medicoToolStripMenuItem1,
            this.funcionariosToolStripMenuItem1});
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            // 
            // pacienteToolStripMenuItem1
            // 
            this.pacienteToolStripMenuItem1.Name = "pacienteToolStripMenuItem1";
            this.pacienteToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.pacienteToolStripMenuItem1.Text = "Paciente";
            this.pacienteToolStripMenuItem1.Click += new System.EventHandler(this.pacienteToolStripMenuItem1_Click);
            // 
            // medicoToolStripMenuItem1
            // 
            this.medicoToolStripMenuItem1.Name = "medicoToolStripMenuItem1";
            this.medicoToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.medicoToolStripMenuItem1.Text = "Médico";
            this.medicoToolStripMenuItem1.Click += new System.EventHandler(this.medicoToolStripMenuItem1_Click);
            // 
            // funcionariosToolStripMenuItem1
            // 
            this.funcionariosToolStripMenuItem1.Name = "funcionariosToolStripMenuItem1";
            this.funcionariosToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.funcionariosToolStripMenuItem1.Text = "Funcionarios";
            this.funcionariosToolStripMenuItem1.Click += new System.EventHandler(this.funcionariosToolStripMenuItem1_Click);
            // 
            // médicoToolStripMenuItem
            // 
            this.médicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.especialidadeToolStripMenuItem});
            this.médicoToolStripMenuItem.Name = "médicoToolStripMenuItem";
            this.médicoToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.médicoToolStripMenuItem.Text = "Médico";
            // 
            // especialidadeToolStripMenuItem
            // 
            this.especialidadeToolStripMenuItem.Name = "especialidadeToolStripMenuItem";
            this.especialidadeToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.especialidadeToolStripMenuItem.Text = "Pesquisar por Especialidade";
            // 
            // administradorToolStripMenuItem
            // 
            this.administradorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.entreComAdministradorToolStripMenuItem});
            this.administradorToolStripMenuItem.Name = "administradorToolStripMenuItem";
            this.administradorToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.administradorToolStripMenuItem.Text = "Administrador";
            // 
            // entreComAdministradorToolStripMenuItem
            // 
            this.entreComAdministradorToolStripMenuItem.Name = "entreComAdministradorToolStripMenuItem";
            this.entreComAdministradorToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.entreComAdministradorToolStripMenuItem.Text = "Login de Administrador";
            this.entreComAdministradorToolStripMenuItem.Click += new System.EventHandler(this.entreComAdministradorToolStripMenuItem_Click);
            // 
            // agendamentoToolStripMenuItem
            // 
            this.agendamentoToolStripMenuItem.Name = "agendamentoToolStripMenuItem";
            this.agendamentoToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.agendamentoToolStripMenuItem.Text = "Agendamento";
            this.agendamentoToolStripMenuItem.Click += new System.EventHandler(this.agendamentoToolStripMenuItem_Click);
            // 
            // panel3
            // 
            this.panel3.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.btSair1);
            this.panel3.Controls.Add(this.btVerificarConsultas);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(0, 85);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(323, 808);
            this.panel3.TabIndex = 60;
            // 
            // btSair1
            // 
            this.btSair1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSair1.BackColor = System.Drawing.Color.Transparent;
            this.btSair1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btSair1.BackgroundImage")));
            this.btSair1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSair1.ForeColor = System.Drawing.Color.Transparent;
            this.btSair1.Location = new System.Drawing.Point(30, 128);
            this.btSair1.Margin = new System.Windows.Forms.Padding(0);
            this.btSair1.Name = "btSair1";
            this.btSair1.Size = new System.Drawing.Size(268, 68);
            this.btSair1.TabIndex = 9;
            this.btSair1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btSair1.UseVisualStyleBackColor = false;
            this.btSair1.Click += new System.EventHandler(this.btSair1_Click);
            // 
            // btVerificarConsultas
            // 
            this.btVerificarConsultas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btVerificarConsultas.BackColor = System.Drawing.Color.Transparent;
            this.btVerificarConsultas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btVerificarConsultas.BackgroundImage")));
            this.btVerificarConsultas.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btVerificarConsultas.ForeColor = System.Drawing.Color.Transparent;
            this.btVerificarConsultas.Location = new System.Drawing.Point(30, 20);
            this.btVerificarConsultas.Margin = new System.Windows.Forms.Padding(0);
            this.btVerificarConsultas.Name = "btVerificarConsultas";
            this.btVerificarConsultas.Size = new System.Drawing.Size(268, 68);
            this.btVerificarConsultas.TabIndex = 0;
            this.btVerificarConsultas.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btVerificarConsultas.UseVisualStyleBackColor = false;
            this.btVerificarConsultas.Click += new System.EventHandler(this.btVerificarConsultas_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1033, 567);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 52);
            this.button2.TabIndex = 127;
            this.button2.Text = "Sair";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(929, 470);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(58, 91);
            this.button3.TabIndex = 126;
            this.button3.Text = "Deletar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(993, 470);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(60, 91);
            this.button5.TabIndex = 125;
            this.button5.Text = "Alterar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(929, 567);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(98, 52);
            this.button6.TabIndex = 124;
            this.button6.Text = "Verificar";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // txtSexo
            // 
            this.txtSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtSexo.FormattingEnabled = true;
            this.txtSexo.Items.AddRange(new object[] {
            "M",
            "F"});
            this.txtSexo.Location = new System.Drawing.Point(568, 234);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(43, 21);
            this.txtSexo.TabIndex = 129;
            // 
            // txtEstadoCivil
            // 
            this.txtEstadoCivil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtEstadoCivil.FormattingEnabled = true;
            this.txtEstadoCivil.Items.AddRange(new object[] {
            "Solteiro(a)",
            "Casado(a)",
            "Viúvo(a)"});
            this.txtEstadoCivil.Location = new System.Drawing.Point(888, 419);
            this.txtEstadoCivil.Name = "txtEstadoCivil";
            this.txtEstadoCivil.Size = new System.Drawing.Size(108, 21);
            this.txtEstadoCivil.TabIndex = 130;
            // 
            // txtGrauDeInstrucao
            // 
            this.txtGrauDeInstrucao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtGrauDeInstrucao.FormattingEnabled = true;
            this.txtGrauDeInstrucao.Items.AddRange(new object[] {
            "Analfabeto",
            "Ensino Fundamental (Completo)",
            "Ensino Médio (Completo)",
            "Ensino Médio (Cursando)",
            "Ensino Médio (Incompleto)",
            "Educação Superior (Completo)",
            "Educação Superior (Cursando)",
            "Educação Superior (Incompleto)",
            "Mestrado (Completo)",
            "Doutorado (Completo)"});
            this.txtGrauDeInstrucao.Location = new System.Drawing.Point(889, 372);
            this.txtGrauDeInstrucao.Name = "txtGrauDeInstrucao";
            this.txtGrauDeInstrucao.Size = new System.Drawing.Size(211, 21);
            this.txtGrauDeInstrucao.TabIndex = 131;
            // 
            // txtCor
            // 
            this.txtCor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtCor.FormattingEnabled = true;
            this.txtCor.Items.AddRange(new object[] {
            "Branco",
            "Preto",
            "Pardo",
            "Amarelo",
            "Indígena",
            "Não sei"});
            this.txtCor.Location = new System.Drawing.Point(1025, 418);
            this.txtCor.Name = "txtCor";
            this.txtCor.Size = new System.Drawing.Size(75, 21);
            this.txtCor.TabIndex = 132;
            // 
            // txtTipoSanguineo
            // 
            this.txtTipoSanguineo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtTipoSanguineo.FormattingEnabled = true;
            this.txtTipoSanguineo.Items.AddRange(new object[] {
            "A+",
            "A-",
            "B+",
            "B-",
            "AB+",
            "AB-",
            "O+",
            "O-"});
            this.txtTipoSanguineo.Location = new System.Drawing.Point(725, 235);
            this.txtTipoSanguineo.Name = "txtTipoSanguineo";
            this.txtTipoSanguineo.Size = new System.Drawing.Size(59, 21);
            this.txtTipoSanguineo.TabIndex = 133;
            // 
            // txtCep
            // 
            this.txtCep.Location = new System.Drawing.Point(859, 238);
            this.txtCep.Mask = "00000-9999";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(70, 20);
            this.txtCep.TabIndex = 152;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(717, 318);
            this.txtTelefone.Mask = "(99) 0000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(81, 20);
            this.txtTelefone.TabIndex = 153;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(842, 319);
            this.txtCelular.Mask = "(99) 00000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(87, 20);
            this.txtCelular.TabIndex = 154;
            // 
            // txtCpf
            // 
            this.txtCpf.Location = new System.Drawing.Point(712, 419);
            this.txtCpf.Mask = "000-999-000-00";
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(100, 20);
            this.txtCpf.TabIndex = 156;
            // 
            // txtRg
            // 
            this.txtRg.Location = new System.Drawing.Point(595, 416);
            this.txtRg.Mask = "00-000-000-0";
            this.txtRg.Name = "txtRg";
            this.txtRg.Size = new System.Drawing.Size(79, 20);
            this.txtRg.TabIndex = 155;
            // 
            // txtNascimento
            // 
            this.txtNascimento.Location = new System.Drawing.Point(446, 235);
            this.txtNascimento.Mask = "00/00/0000";
            this.txtNascimento.Name = "txtNascimento";
            this.txtNascimento.Size = new System.Drawing.Size(68, 20);
            this.txtNascimento.TabIndex = 157;
            this.txtNascimento.ValidatingType = typeof(System.DateTime);
            this.txtNascimento.TypeValidationCompleted += new System.Windows.Forms.TypeValidationEventHandler(this.txtNascimento_TypeValidationCompleted);
            // 
            // txtEstado
            // 
            this.txtEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtEstado.FormattingEnabled = true;
            this.txtEstado.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP ",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.txtEstado.Location = new System.Drawing.Point(609, 314);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(54, 21);
            this.txtEstado.TabIndex = 158;
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(898, 197);
            this.txtCodigo.Mask = "0000";
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(31, 20);
            this.txtCodigo.TabIndex = 159;
            this.txtCodigo.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtCodigo_MaskInputRejected);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(367, 663);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(195, 16);
            this.label12.TabIndex = 160;
            this.label12.Text = "(*) CAMPOS OBRIGATÓRIOS !!";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Location = new System.Drawing.Point(490, 85);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(439, 85);
            this.panel1.TabIndex = 161;
            // 
            // Form3
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 688);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.txtEstado);
            this.Controls.Add(this.txtNascimento);
            this.Controls.Add(this.txtCpf);
            this.Controls.Add(this.txtRg);
            this.Controls.Add(this.txtCelular);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.txtCep);
            this.Controls.Add(this.txtTipoSanguineo);
            this.Controls.Add(this.txtCor);
            this.Controls.Add(this.txtGrauDeInstrucao);
            this.Controls.Add(this.txtEstadoCivil);
            this.Controls.Add(this.txtSexo);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtProfissao);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtNaturalidade);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtCidade);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtPai);
            this.Controls.Add(this.Pai);
            this.Controls.Add(this.txtMae);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtConjuge);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBairro);
            this.Controls.Add(this.txtEndereco);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Endereço);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btImagem);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro Paciente";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Button btImagem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Endereço;
        private System.Windows.Forms.TextBox txtConjuge;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMae;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPai;
        private System.Windows.Forms.Label Pai;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtNaturalidade;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtProfissao;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btSair;
        private System.Windows.Forms.Button btRelatorio;
        private System.Windows.Forms.Button btAdm;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pesquisarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funcionariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacienteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem medicoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem funcionariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem médicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administradorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agendamentoToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btSair1;
        private System.Windows.Forms.Button btVerificarConsultas;
        private System.Windows.Forms.ToolStripMenuItem principalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem telaPrincipalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem especialidadeToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ToolStripMenuItem entreComAdministradorToolStripMenuItem;
        private System.Windows.Forms.ComboBox txtSexo;
        private System.Windows.Forms.ComboBox txtEstadoCivil;
        private System.Windows.Forms.ComboBox txtGrauDeInstrucao;
        private System.Windows.Forms.ComboBox txtCor;
        private System.Windows.Forms.ComboBox txtTipoSanguineo;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.MaskedTextBox txtCpf;
        private System.Windows.Forms.MaskedTextBox txtRg;
        private System.Windows.Forms.MaskedTextBox txtNascimento;
        private System.Windows.Forms.ComboBox txtEstado;
        private System.Windows.Forms.MaskedTextBox txtCodigo;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
    }
}