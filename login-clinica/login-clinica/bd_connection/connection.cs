﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace login_clinica.bd_connection
{
    public class connection
    {
        SqlConnection dbbanco = new SqlConnection(); //biblioteca banco\\

        public connection()
        {

            dbbanco.ConnectionString = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";//nome banco //
        }

        // (3) Inicio dos blocos de tratativas de conexão //
        public SqlConnection Conectar() // (3.1) Metodo conectar//
        {
            if(dbbanco.State == System.Data.ConnectionState.Closed)
            {
                dbbanco.Open();
            }
            return dbbanco;
        }
        public void Desconectar()
        {
            if(dbbanco.State == System.Data.ConnectionState.Open)
            {
                dbbanco.Close();
            }
        }
    }
}

// fim dos blocoa de tratativas de conexão ///