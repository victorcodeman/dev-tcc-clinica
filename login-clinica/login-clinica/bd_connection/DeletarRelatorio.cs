﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica.bd_connection
{
    class DeletarRelatorio
    {
        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();
        public DeletarRelatorio(int cod_relatorio, String tipo_relatorio, DateTime data_Relatorio, int Codigo_Paciente, int Codigo_medico, String relatorio)
        {

            cmd.CommandText = "Delete from Cadastro_Relatorio where cod_relatorio=@cod_relatorio";

            cmd.Parameters.AddWithValue("@cod_relatorio", cod_relatorio);
            cmd.Parameters.AddWithValue("@tipo_relatorio", tipo_relatorio);
            cmd.Parameters.AddWithValue("@data_Relatorio", data_Relatorio);
            cmd.Parameters.AddWithValue("@Codigo_Paciente", Codigo_Paciente);
            cmd.Parameters.AddWithValue("@Codigo_medico", Codigo_medico);
            cmd.Parameters.AddWithValue("@relatorio", relatorio);


            try
            {

                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Cadastro Efetuado com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }

        }
    }
}
