﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica.bd_connection
{
    public class AlterarProntuario
    {
        SqlCommand cmd = new SqlCommand();
        connection dbbanco = new connection();

        public AlterarProntuario(int cod_prontuario,float altura, float peso, String medicamento, String diaginosticos, String alergias, String hipotese_diagnostica, String problemas_Saude,int Cod_pacient)
        {


            cmd.CommandText = "Update prontuario set altura=@altura,peso=@peso,medicamento=@medicamento,diaginosticos=@diaginosticos,alergias=@alergias,hipotese_diagnostica=@hipotese_diagnostica,problemas_Saude=@problemas_Saude,Cod_pacient = @Cod_pacient where cod_prontuario=@cod_prontuario";
            cmd.Parameters.AddWithValue("@cod_prontuario", cod_prontuario);
            cmd.Parameters.AddWithValue("@altura", altura);
            cmd.Parameters.AddWithValue("@peso", peso);
            cmd.Parameters.AddWithValue("@medicamento", medicamento);
            cmd.Parameters.AddWithValue("@diaginosticos", diaginosticos);
            cmd.Parameters.AddWithValue("@alergias", alergias);
            cmd.Parameters.AddWithValue("@hipotese_diagnostica", hipotese_diagnostica);
            cmd.Parameters.AddWithValue("@problemas_Saude", problemas_Saude);
            cmd.Parameters.AddWithValue("@Cod_pacient", Cod_pacient);









            try
            {
                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Alteração Efetuada com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }
        }
    }
}
