﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using login_clinica.bd_connection;


namespace login_clinica
{
    class PesquisaFuncionario
    {

        connection conexao = new connection();
      
        SqlCommand cmd = new SqlCommand();

        public PesquisaFuncionario(String nome, DataGridView data)
        {
            try
            {
                SqlDataAdapter inst = new SqlDataAdapter("select * from Cadastro_Funcionario  where Cod_funcionario   like'" + nome + "%'", conexao.Conectar().ConnectionString);
                DataTable tabela = new DataTable();
                inst.Fill(tabela);



                data.DataSource = tabela;

                conexao.Desconectar();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Codigo não encontrado");
            }
        }
            public void PesquisarNome(String nome, DataGridView data)
        {
                try
                {
                    SqlDataAdapter inst = new SqlDataAdapter("select * from Cadastro_Funcionario  where nome_funcionario   like'" + nome + "%'", conexao.Conectar().ConnectionString);
                    DataTable tabela = new DataTable();
                    inst.Fill(tabela);



                    data.DataSource = tabela;

                    conexao.Desconectar();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Codigo não encontrado");
                }


            }
        public void PesquisarCargo(String nome, DataGridView data)
        {
            try
            {
                SqlDataAdapter inst = new SqlDataAdapter("select * from Cadastro_Funcionario  where cargo like'" + nome + "%'", conexao.Conectar().ConnectionString);
                DataTable tabela = new DataTable();
                inst.Fill(tabela);



                data.DataSource = tabela;

                conexao.Desconectar();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Codigo não encontrado");
            }


        }
        public void PesquisarEspecialidade(String nome, DataGridView data)
        {
            try
            {
                SqlDataAdapter inst = new SqlDataAdapter("select * from Cadastro_Funcionario  where especialidade   like'" + nome + "%'", conexao.Conectar().ConnectionString);
                DataTable tabela = new DataTable();
                inst.Fill(tabela);



                data.DataSource = tabela;

                conexao.Desconectar();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Codigo não encontrado");
            }


        }
    
    }
}
