﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica.bd_connection
{
    public class CadConsulta
    {
        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();


        public CadConsulta(int cod_consulta,String Tipo_Agendamento, String Tipo_Consulta,DateTime Data_Consulta,DateTime DataAte, int Medico,int Cod_pacient, DateTime Data_Agendamento,String Status_Consulta,String Data)
        {
            cmd.CommandText = "insert into Agendamento_consulta(cod_consulta,Tipo_Agendamento,Tipo_Consulta,Data_Consulta,DataAte,Medico,Cod_pacient,Data_Agendamento,Status_Consulta,Data) values (@cod_consulta,@Tipo_Agendamento,@Tipo_Consulta,@Data_Consulta,@DataAte,@Medico,@Cod_pacient,@Data_Agendamento,@Status_Consulta,@Data)";

            cmd.Parameters.AddWithValue("@cod_consulta", cod_consulta);
            cmd.Parameters.AddWithValue("@Tipo_Agendamento", Tipo_Agendamento);
            cmd.Parameters.AddWithValue("@Tipo_Consulta", Tipo_Consulta);
            cmd.Parameters.AddWithValue("@Data_Consulta", Data_Consulta);
            cmd.Parameters.AddWithValue("@DataAte", DataAte); 
            cmd.Parameters.AddWithValue("@Medico", Medico);
            cmd.Parameters.AddWithValue("@Cod_pacient", Cod_pacient);
            cmd.Parameters.AddWithValue("@Status_Consulta", Status_Consulta);
            cmd.Parameters.AddWithValue("@Data_Agendamento", Data_Agendamento);
            
            cmd.Parameters.AddWithValue("@Data", Data);
           


            try
            {
                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Cadastro Efetuado com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }


        }
    }
}
