﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica.bd_connection
{
    class AlterarHistoricoAgendamento
    {

        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();
        public AlterarHistoricoAgendamento(int cod_historico, String Tipo_Agendamento, String Tipo_Consulta, DateTime Data_Consulta,DateTime DataAte, int Medico, int Cod_pacient, DateTime Data_Agendamento, String Status_Consulta, String Data)
        {
            cmd.CommandText = "Update Historico_Agendamento set Tipo_Agendamento=@Tipo_Agendamento,Tipo_Consulta=@Tipo_Consulta,Data_Consulta=@Data_Consulta,DataAte=@DataAte,Medico=@Medico,Cod_pacient=@Cod_pacient,Data_Agendamento=@Data_Agendamento,Status_Consulta=@Status_Consulta,Data=@Data where cod_historico= @cod_historico";


            cmd.Parameters.AddWithValue("@cod_historico", cod_historico);
            cmd.Parameters.AddWithValue("@Tipo_Agendamento", Tipo_Agendamento);
            cmd.Parameters.AddWithValue("@Tipo_Consulta", Tipo_Consulta);
            cmd.Parameters.AddWithValue("@Data_Consulta", Data_Consulta);
            cmd.Parameters.AddWithValue("@DataAte", DataAte);
            cmd.Parameters.AddWithValue("@Medico", Medico);
            cmd.Parameters.AddWithValue("@Cod_pacient", Cod_pacient);
            cmd.Parameters.AddWithValue("@Status_Consulta", Status_Consulta);
            cmd.Parameters.AddWithValue("@Data_Agendamento", Data_Agendamento);

            cmd.Parameters.AddWithValue("@Data", Data);

            try
            {
                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Alteração Efetuada com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }
        }
    }
}
