﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica.bd_connection
{
    class DeletarMedico
    {


        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();

        public DeletarMedico(int cod_medico, string nome_medico, DateTime Nascimento, string sexo, string Tipo_Sanguinio, string endereco, string bairro, string cidade, string estado, Int64 cep, Int64 telefone, Int64 celular, Int64 rg, Int64 cpf, string estado_civil, int crm_medico, string email, string naturalidade, string cor, string especialidade, string conjuge)
        {
            cmd.CommandText = "Delete from Cadastro_Medico where cod_medico=@cod_medico";
            cmd.Parameters.AddWithValue("@cod_medico", cod_medico);
            cmd.Parameters.AddWithValue("@nome_medico", nome_medico);
            cmd.Parameters.AddWithValue("@Nascimento", Nascimento);
            cmd.Parameters.AddWithValue("@sexo", sexo);
            cmd.Parameters.AddWithValue("@Tipo_Sanguinio", Tipo_Sanguinio);
            cmd.Parameters.AddWithValue("@endereco", endereco);
            cmd.Parameters.AddWithValue("@bairro", bairro);
            cmd.Parameters.AddWithValue("@cidade", cidade);
            cmd.Parameters.AddWithValue("@estado", estado);
            cmd.Parameters.AddWithValue("@cep", cep);
            cmd.Parameters.AddWithValue("@telefone", telefone);
            cmd.Parameters.AddWithValue("@celular", celular);
          
            cmd.Parameters.AddWithValue("@rg", rg);
            cmd.Parameters.AddWithValue("@cpf", cpf);
            cmd.Parameters.AddWithValue("@estado_civil", estado_civil);
            cmd.Parameters.AddWithValue("@crm_medico", crm_medico);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@naturalidade", naturalidade);
            cmd.Parameters.AddWithValue("@cor", cor);
            cmd.Parameters.AddWithValue("@especialidade", especialidade);
            cmd.Parameters.AddWithValue("@conjuge", conjuge);




            try
            {

                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Medico Deletado com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }


        }
    }

}
    

