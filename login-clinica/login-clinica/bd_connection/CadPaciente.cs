﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica.bd_connection
{
    public class CadPaciente
    {


      
        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();
       



        public CadPaciente(int Cod_paciente, String nome, DateTime Nascimento,String sexo, String Tipo_Sanguinio, String endereco, String bairro, String cidade, String estado, Int64 cep, Int64 telefone, Int64 celular, Int64 rg, Int64 cpf, String estado_civil, String profissao, String email, String naturalidade, String cor, String Grau_de_instrucao, String conjuge, String mae, String pai)
        {
                 


            cmd.CommandText = "insert into Cadastro_Paciente(Cod_paciente,nome,Nascimento,sexo,Tipo_Sanguinio,endereco,bairro,cidade,estado,cep,telefone,celular,rg,cpf,estado_civil,profissao,email,naturalidade,cor,Grau_de_instrucao,conjuge,mae,pai) values (@Cod_paciente,@nome,@Nascimento,@sexo,@Tipo_Sanguinio,@endereco,@bairro,@cidade,@estado,@cep,@telefone,@celular,@rg,@cpf,@estado_civil,@profissao,@email,@naturalidade,@cor,@Grau_de_instrucao,@conjuge,@mae,@pai)";
            cmd.Parameters.AddWithValue("@Cod_paciente", Cod_paciente);
            cmd.Parameters.AddWithValue("@nome", nome);
            cmd.Parameters.AddWithValue("@Nascimento", Nascimento);
            cmd.Parameters.AddWithValue("@sexo", sexo);
            cmd.Parameters.AddWithValue("@Tipo_Sanguinio", Tipo_Sanguinio);
            cmd.Parameters.AddWithValue("@endereco", endereco);
            cmd.Parameters.AddWithValue("@bairro", bairro);
            cmd.Parameters.AddWithValue("@cidade", cidade);
            cmd.Parameters.AddWithValue("@estado", estado);
            cmd.Parameters.AddWithValue("@cep", cep);
            cmd.Parameters.AddWithValue("@telefone", telefone);
            cmd.Parameters.AddWithValue("@celular", celular);
            
            cmd.Parameters.AddWithValue("@rg", rg);
            cmd.Parameters.AddWithValue("@cpf", cpf);
            cmd.Parameters.AddWithValue("@estado_civil", estado_civil);
            cmd.Parameters.AddWithValue("@profissao", profissao);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@naturalidade", naturalidade);
            cmd.Parameters.AddWithValue("@cor", cor);
            cmd.Parameters.AddWithValue("@Grau_de_instrucao", Grau_de_instrucao);
            cmd.Parameters.AddWithValue("@conjuge", conjuge);
            cmd.Parameters.AddWithValue("@mae", mae);
            cmd.Parameters.AddWithValue("@pai", pai);








            try
            {
                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Cadastro Efetuado com Sucesso!!!!!!");
          


            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();
                
               
            }

        }

   
    }
}

