﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public class DeletarFuncionario
    {
        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();


        public DeletarFuncionario(int Cod_funcionario, String nome_funcionario, DateTime Nascimento, String sexo, String Tipo_Sanguinio, String endereco, String bairro, String cidade, String estado, Int64 cep, Int64 telefone, Int64 celular, Int64 rg, Int64 cpf, String estado_civil, String email, String naturalidade, String cor, String Grau_de_instrucao, String formacao, String especialidade, String cargo, String conjuge)
        {



            cmd.CommandText = "Delete from Cadastro_Funcionario where Cod_funcionario= @Cod_funcionario";
            cmd.Parameters.AddWithValue("@Cod_funcionario", Cod_funcionario);
            cmd.Parameters.AddWithValue("@nome_funcionario", nome_funcionario);
            cmd.Parameters.AddWithValue("@Nascimento", Nascimento);
            cmd.Parameters.AddWithValue("@sexo", sexo);
            cmd.Parameters.AddWithValue("@Tipo_Sanguinio", Tipo_Sanguinio);
            cmd.Parameters.AddWithValue("@endereco", endereco);
            cmd.Parameters.AddWithValue("@bairro", bairro);
            cmd.Parameters.AddWithValue("@cidade", cidade);
            cmd.Parameters.AddWithValue("@estado", estado);
            cmd.Parameters.AddWithValue("@cep", cep);
            cmd.Parameters.AddWithValue("@telefone", telefone);
            cmd.Parameters.AddWithValue("@celular", celular);
           
            cmd.Parameters.AddWithValue("@rg", rg);
            cmd.Parameters.AddWithValue("@cpf", cpf);
            cmd.Parameters.AddWithValue("@estado_civil", estado_civil);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@naturalidade", naturalidade);
            cmd.Parameters.AddWithValue("@cor", cor);
            cmd.Parameters.AddWithValue("@Grau_de_instrucao", Grau_de_instrucao);
            cmd.Parameters.AddWithValue("@formacao", formacao);
            cmd.Parameters.AddWithValue("@especialidade", especialidade);
            cmd.Parameters.AddWithValue("@cargo", cargo);

            cmd.Parameters.AddWithValue("@conjuge", conjuge);








            try
            {

                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Cadastro Deletado com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }
        }
    }
}
