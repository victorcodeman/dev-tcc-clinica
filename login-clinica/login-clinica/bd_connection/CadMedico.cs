﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica.bd_connection
{
    public class CadMedico
    {

        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();


        public CadMedico(int cod_medico, String nome_medico, DateTime Nascimento, String sexo, String Tipo_Sanguinio, String endereco, String bairro, String cidade, String estado, Int64 cep, Int64 telefone, Int64 celular, Int64 rg, Int64 cpf, String estado_civil, int crm_medico, String email, String naturalidade, String cor, String especialidade, String conjuge)
        {
            cmd.CommandText = "insert into Cadastro_Medico(cod_medico,nome_medico,Nascimento,sexo,Tipo_Sanguinio,endereco,bairro,cidade,estado,cep,telefone,celular,rg,cpf,estado_civil,crm_medico,email,naturalidade,cor,especialidade,conjuge) values (@cod_medico,@nome_medico,@Nascimento,@sexo,@Tipo_Sanguinio,@endereco,@bairro,@cidade,@estado,@cep,@telefone,@celular,@rg,@cpf,@estado_civil,@crm_medico,@email,@naturalidade,@cor,@especialidade,@conjuge)";
            cmd.Parameters.AddWithValue("@cod_medico", cod_medico);
            cmd.Parameters.AddWithValue("@nome_medico", nome_medico);
            cmd.Parameters.AddWithValue("@Nascimento", Nascimento);
            cmd.Parameters.AddWithValue("@sexo", sexo);
            cmd.Parameters.AddWithValue("@Tipo_Sanguinio", Tipo_Sanguinio);
            cmd.Parameters.AddWithValue("@endereco", endereco);
            cmd.Parameters.AddWithValue("@bairro", bairro);
            cmd.Parameters.AddWithValue("@cidade", cidade);
            cmd.Parameters.AddWithValue("@estado", estado);
            cmd.Parameters.AddWithValue("@cep", cep);
            cmd.Parameters.AddWithValue("@telefone", telefone);
            cmd.Parameters.AddWithValue("@celular", celular);
            
            cmd.Parameters.AddWithValue("@rg", rg);
            cmd.Parameters.AddWithValue("@cpf", cpf);
            cmd.Parameters.AddWithValue("@estado_civil", estado_civil);
            cmd.Parameters.AddWithValue("@crm_medico", crm_medico);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@naturalidade", naturalidade);
            cmd.Parameters.AddWithValue("@cor", cor);
            cmd.Parameters.AddWithValue("@especialidade", especialidade);
            cmd.Parameters.AddWithValue("@conjuge", conjuge);




            try
            {

                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Cadastro Efetuado com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }

        }
    }
}
