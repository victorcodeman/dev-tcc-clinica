﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace login_clinica.bd_connection
{
    class login_entrar
    {
       
        public bool existe = false;
        public String statusConexao = "";
        SqlCommand cmd = new SqlCommand();
        connection dbbanco = new connection();
        SqlDataReader dr;

        public bool verificaLogin(String login, String senha)
        {
            cmd.CommandText = "select *from Usuario where nome_usuario = @login and senha_usuario=@senha";
            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@senha", senha);

            try
            {
                cmd.Connection = dbbanco.Conectar();
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    existe = true;
                }
            }
            catch (SqlException)
            {
                this.statusConexao = "Erro com o banco de dados!";


            }
            return existe;
        }
        public bool verificaLogin2(String login, String senha)
        {
            cmd.CommandText = "select *from Usuario where cd_perfil = 1 and nome_usuario = @login and senha_usuario=@senha";
            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@senha", senha);

            try
            {
                cmd.Connection = dbbanco.Conectar();
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    existe = true;
                }
                 
    }
            catch (SqlException)
            {
                this.statusConexao = "Erro com o banco de dados!";


            }
            return existe;
        }
        public bool verificaLogin3(String login, String senha)
        {
            cmd.CommandText = "select *from Usuario where cd_perfil = 3 and nome_usuario = @login and senha_usuario=@senha";
            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@senha", senha);

            try
            {
                cmd.Connection = dbbanco.Conectar();
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    existe = true;
                    

                }
               

            }
            catch (SqlException)
            {
                this.statusConexao = "Erro com o banco de dados!";


            }
            return existe;
             
            

        }
    }

}
