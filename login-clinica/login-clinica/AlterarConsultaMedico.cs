﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
     class AlterarConsultaMedico
    {
        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();
       public AlterarConsultaMedico(int cod_consulta,String Status_Consulta)
        {
            cmd.CommandText = "Update Agendamento_consulta set cod_consulta= @cod_consulta,Status_Consulta=@Status_Consulta where cod_consulta= @cod_consulta";


            cmd.Parameters.AddWithValue("@cod_consulta", cod_consulta);
          
            cmd.Parameters.AddWithValue("@Status_Consulta", Status_Consulta);
           


            try
            {
                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Alteração Efetuada com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }

        }
    }
}
