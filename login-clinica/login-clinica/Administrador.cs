﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class Administrador : Form
    {
        public Administrador()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txtUsuario.Text == string.Empty || txtCodigo.Text == string.Empty || txtLogin.TextLength < 3 || txtSenha.TextLength < 3 || txtVerificarSenha.TextLength < 3)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*)!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
             int cd_perfil = 0;
            if (txtUsuario.Text == "Administrador")
            {
                cd_perfil = 1;
                String tipo_perfil = "Administrador";
                CadUsuario cad = new CadUsuario(int.Parse(txtCodigo.Text),cd_perfil, tipo_perfil, txtSenha.Text, txtLogin.Text, txtVerificarSenha.Text);
                Close();
                Administrador p1 = new Administrador();
                p1.Show();
            }
            else if(txtUsuario.Text == "Funcionário")
            {
                cd_perfil = 2;
                String tipo_perfil = "Funcionário";
                CadUsuario cad = new CadUsuario(int.Parse(txtCodigo.Text), cd_perfil, tipo_perfil, txtSenha.Text, txtLogin.Text, txtVerificarSenha.Text);
                Close();
                Administrador p1 = new Administrador();
                p1.Show();
            }
            else if(txtUsuario.Text == "Médico")
            {
                cd_perfil = 3;
                String tipo_perfil = "Médico";
                CadUsuario cad = new CadUsuario(int.Parse(txtCodigo.Text), cd_perfil, tipo_perfil, txtSenha.Text, txtLogin.Text, txtVerificarSenha.Text);
                Close();
                Administrador p1 = new Administrador();
                p1.Show();
            }

        }

        private void entrarComoAdiministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int cd_perfil = 0;
            if (txtUsuario.Text == "Administrador")
            {
                cd_perfil = 1;
                String tipo_perfil = "Administrador";
                AlterarUsuario a1 = new AlterarUsuario(int.Parse(txtCodigo.Text), cd_perfil, tipo_perfil, txtSenha.Text, txtLogin.Text, txtVerificarSenha.Text);
                Close();
                Administrador p1 = new Administrador();
                p1.Show();
            }
            else
            {
                cd_perfil = 2;
                String tipo_perfil = "Funcionário";
                AlterarUsuario a1 = new AlterarUsuario(int.Parse(txtCodigo.Text), cd_perfil, tipo_perfil, txtSenha.Text, txtLogin.Text, txtVerificarSenha.Text);
                Close();
                Administrador p1 = new Administrador();
                p1.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int cd_perfil = 0;
            if (txtUsuario.Text == "Administrador")
            {
                cd_perfil = 1;
                String tipo_perfil = "Administrador";
                DeletarUsuario a1 = new DeletarUsuario(int.Parse(txtCodigo.Text), cd_perfil, tipo_perfil, txtSenha.Text, txtLogin.Text, txtVerificarSenha.Text);
                Close();
                Administrador p1 = new Administrador();
                p1.Show();
            }
            else
            {
                cd_perfil = 2;
                String tipo_perfil = "Funcionário";
                DeletarUsuario a1 = new DeletarUsuario(int.Parse(txtCodigo.Text), cd_perfil, tipo_perfil, txtSenha.Text, txtLogin.Text, txtVerificarSenha.Text);
                Close();
                Administrador p1 = new Administrador();
                p1.Show();
            }
        }
        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        private string strSql1 = string.Empty;
        private void button6_Click(object sender, EventArgs e)
        {
            strSql = "SELECT * FROM Usuario WHERE cd_usuario = @cd_usuario";

            Sqlcon = new SqlConnection(Strcon);
            SqlCommand cmd = new SqlCommand(strSql, Sqlcon);


            cmd.Parameters.Add("@cd_usuario", SqlDbType.VarChar).Value = txtCodigo.Text;


            try
            {


                if (txtCodigo.Text == String.Empty)
                {
                    throw new Exception("você precisa digitar o codigo do paciente!!!");
                }
                Sqlcon.Open();
                SqlDataReader reader = cmd.ExecuteReader();



                if (reader.HasRows == false)
                {

                    throw new Exception("prontuario cadastrado com sucesso!!!");

                }
                reader.Read();

        
                    txtCodigo.Text = Convert.ToString(reader["cd_usuario"]);
                txtUsuario.Text = Convert.ToString(reader["tipo_perfil"]);
                txtSenha.Text = Convert.ToString(reader["senha_usuario"]);
                txtLogin.Text = Convert.ToString(reader["nome_usuario"]);

               








            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                Sqlcon.Close();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {


                SqlConnection dbConecao = new SqlConnection(@"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True");
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Usuario where cd_usuario like'" + textBox1.Text + "%'", dbConecao);

                DataTable tabela = new DataTable();



                instrucao.Fill(tabela);
                tabela.Columns[0].ColumnName = "cd_usuario";
               
              
               



                dataGridView1.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void especialidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade m1 = new pesquisaMedicoEspecialidade();
            m1.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 c1 = new Form3();
            c1.Show();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico m1 = new Cadastro_medico();
            m1.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario f1 = new Cadastro_funcionario();
            f1.Show();
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente p1 = new Pesquisar_Paciente();
            p1.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico m1 = new pesquisar_Medico();
            m1.Show();
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 p = new Form2();
            p.Show();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btVerificarConsultas_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {
            txtLogin.MaxLength = 30;
        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {
            txtSenha.MaxLength = 15;
        }

        private void txtVerificarSenha_TextChanged(object sender, EventArgs e)
        {
            txtVerificarSenha.MaxLength = 15;
        }

        private void Administrador_Load(object sender, EventArgs e)
        {

        }
    }
    }

