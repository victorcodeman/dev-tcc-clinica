﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class Login : Form
    {
        const string string_conexao = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";

        SqlConnection conexao = new SqlConnection(string_conexao);
        public Login()
        {
            InitializeComponent();
        }

        private void btEntrar_Click(object sender, EventArgs e)
        {
            
            Controle.Controle ctrl = new Controle.Controle();
            ctrl.Administrador(txtLogin.Text, txtSenha.Text);      // ele pega oq foi digitado no campo de texto e joga no acessar que é o metodo da classe controle
            if (ctrl.statusConexao.Equals(""))
            {
                if (ctrl.existe)
                {
                 
                   
                        MessageBox.Show("Logado", "acessando", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.Visible = false;    

                        Administrador principal = new Administrador();
                        principal.Show();

                   
                }

                else
                {
                    MessageBox.Show("Usuário ou senha incorretos!", "Não encontrado", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            }
            else
            {
                MessageBox.Show(ctrl.statusConexao);
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
