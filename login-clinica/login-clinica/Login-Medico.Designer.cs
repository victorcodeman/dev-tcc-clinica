﻿namespace login_clinica
{
    partial class Login_Medico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login_Medico));
            this.txtPesquisarPaciente = new System.Windows.Forms.TextBox();
            this.btPesquisarPaciente = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.txtPesquisar = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.txtStatusAgendamento = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCodigoConsulta = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.btSair1 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btAdm = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPesquisarPaciente
            // 
            this.txtPesquisarPaciente.Location = new System.Drawing.Point(359, 158);
            this.txtPesquisarPaciente.Name = "txtPesquisarPaciente";
            this.txtPesquisarPaciente.Size = new System.Drawing.Size(624, 20);
            this.txtPesquisarPaciente.TabIndex = 202;
            // 
            // btPesquisarPaciente
            // 
            this.btPesquisarPaciente.Location = new System.Drawing.Point(988, 154);
            this.btPesquisarPaciente.Name = "btPesquisarPaciente";
            this.btPesquisarPaciente.Size = new System.Drawing.Size(124, 24);
            this.btPesquisarPaciente.TabIndex = 201;
            this.btPesquisarPaciente.Text = "Pesquisar Pacientes";
            this.btPesquisarPaciente.UseVisualStyleBackColor = true;
            this.btPesquisarPaciente.Click += new System.EventHandler(this.btPesquisarPaciente_Click);
            // 
            // dataGridView3
            // 
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(359, 184);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(753, 133);
            this.dataGridView3.TabIndex = 200;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(878, 599);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 35);
            this.button2.TabIndex = 204;
            this.button2.Text = "Verificar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.Location = new System.Drawing.Point(850, 637);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(291, 60);
            this.panel4.TabIndex = 203;
            // 
            // btPesquisar
            // 
            this.btPesquisar.Location = new System.Drawing.Point(559, 322);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(120, 25);
            this.btPesquisar.TabIndex = 207;
            this.btPesquisar.Text = "Codigo Consulta";
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // txtPesquisar
            // 
            this.txtPesquisar.Location = new System.Drawing.Point(359, 325);
            this.txtPesquisar.Name = "txtPesquisar";
            this.txtPesquisar.Size = new System.Drawing.Size(198, 20);
            this.txtPesquisar.TabIndex = 206;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(359, 353);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(753, 240);
            this.dataGridView1.TabIndex = 205;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(998, 599);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(114, 35);
            this.button3.TabIndex = 209;
            this.button3.Text = "Alterar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtStatusAgendamento
            // 
            this.txtStatusAgendamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtStatusAgendamento.FormattingEnabled = true;
            this.txtStatusAgendamento.Items.AddRange(new object[] {
            "Cancelado",
            "Feito",
            "Andamento",
            "Faltou"});
            this.txtStatusAgendamento.Location = new System.Drawing.Point(1005, 325);
            this.txtStatusAgendamento.Name = "txtStatusAgendamento";
            this.txtStatusAgendamento.Size = new System.Drawing.Size(107, 21);
            this.txtStatusAgendamento.TabIndex = 214;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(868, 328);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 13);
            this.label11.TabIndex = 213;
            this.label11.Text = "Status do Agendamento:";
            // 
            // txtCodigoConsulta
            // 
            this.txtCodigoConsulta.Location = new System.Drawing.Point(794, 323);
            this.txtCodigoConsulta.Name = "txtCodigoConsulta";
            this.txtCodigoConsulta.Size = new System.Drawing.Size(68, 20);
            this.txtCodigoConsulta.TabIndex = 212;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(685, 328);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 13);
            this.label18.TabIndex = 211;
            this.label18.Text = "Codigo Consulta:";
            // 
            // panel3
            // 
            this.panel3.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.button8);
            this.panel3.Controls.Add(this.btSair1);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(0, 57);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(323, 641);
            this.panel3.TabIndex = 216;
            // 
            // button8
            // 
            this.button8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button8.BackColor = System.Drawing.Color.Transparent;
            this.button8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button8.BackgroundImage")));
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.ForeColor = System.Drawing.Color.Transparent;
            this.button8.Location = new System.Drawing.Point(30, 24);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(268, 68);
            this.button8.TabIndex = 43;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btSair1
            // 
            this.btSair1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSair1.BackColor = System.Drawing.Color.Transparent;
            this.btSair1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btSair1.BackgroundImage")));
            this.btSair1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSair1.ForeColor = System.Drawing.Color.Transparent;
            this.btSair1.Location = new System.Drawing.Point(30, 116);
            this.btSair1.Margin = new System.Windows.Forms.Padding(0);
            this.btSair1.Name = "btSair1";
            this.btSair1.Size = new System.Drawing.Size(268, 68);
            this.btSair1.TabIndex = 9;
            this.btSair1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btSair1.UseVisualStyleBackColor = false;
            this.btSair1.Click += new System.EventHandler(this.btSair1_Click);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.button6);
            this.panel5.Controls.Add(this.btAdm);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1140, 58);
            this.panel5.TabIndex = 220;
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.Location = new System.Drawing.Point(141, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 52);
            this.button1.TabIndex = 13;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button6
            // 
            this.button6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button6.BackgroundImage")));
            this.button6.Location = new System.Drawing.Point(73, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(62, 52);
            this.button6.TabIndex = 10;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btAdm
            // 
            this.btAdm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btAdm.BackgroundImage")));
            this.btAdm.Location = new System.Drawing.Point(5, 2);
            this.btAdm.Name = "btAdm";
            this.btAdm.Size = new System.Drawing.Size(62, 52);
            this.btAdm.TabIndex = 3;
            this.btAdm.UseVisualStyleBackColor = true;
            this.btAdm.Click += new System.EventHandler(this.btAdm_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Location = new System.Drawing.Point(583, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(312, 85);
            this.panel1.TabIndex = 221;
            // 
            // Login_Medico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 696);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.txtStatusAgendamento);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtCodigoConsulta);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btPesquisar);
            this.Controls.Add(this.txtPesquisar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.txtPesquisarPaciente);
            this.Controls.Add(this.btPesquisarPaciente);
            this.Controls.Add(this.dataGridView3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Login_Medico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login_Medico";
            this.Load += new System.EventHandler(this.Login_Medico_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPesquisarPaciente;
        private System.Windows.Forms.Button btPesquisarPaciente;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.TextBox txtPesquisar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox txtStatusAgendamento;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCodigoConsulta;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btSair1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btAdm;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button8;
    }
}