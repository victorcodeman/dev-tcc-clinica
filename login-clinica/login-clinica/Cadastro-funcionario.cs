﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace login_clinica
{

   
    public partial class Cadastro_funcionario : Form
    {
        Bitmap bmp;
        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        public Cadastro_funcionario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btImagem_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                string nome = openFileDialog1.FileName;

                bmp = new Bitmap(nome);

                pictureBox1.Image = bmp;


            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtNome.TextLength < 4 || txtCodigo.Text == string.Empty || !txtCpf.MaskCompleted || !txtNascimento.MaskCompleted || !txtRg.MaskCompleted || !txtTelefone.MaskCompleted || !txtCep.MaskCompleted || !txtCelular.MaskCompleted || txtEndereco.TextLength < 4 || txtBairro.TextLength < 4 || txtEstado.Text == string.Empty || txtCidade.TextLength < 4)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*) para deletar o cadastro do funcionario!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

           
                


            else {
                Regex regex = new Regex(@"[^\d]");
                DeletarFuncionario fun1 = new DeletarFuncionario(int.Parse(txtCodigo.Text), txtNome.Text, Convert.ToDateTime(txtNascimento.Text), txtSexo.Text, txtTipoSanguineo.Text, txtEndereco.Text, txtBairro.Text, txtCidade.Text, txtEstado.Text, Convert.ToInt64(regex.Replace(txtCep.Text, "")), Convert.ToInt64(regex.Replace(txtTelefone.Text, "")), Convert.ToInt64(regex.Replace(txtCelular.Text, "")), Convert.ToInt64(regex.Replace(txtRg.Text, "")), Convert.ToInt64(regex.Replace(txtCpf.Text, "")), txtEstadoCivil.Text, txtEmail.Text, txtNaturalidade.Text, txtCor.Text, txtGrauDeInstrucao.Text, txtFormacao.Text, txtEspecialidade.Text, txtCargo.Text, txtConjuge.Text);
                MemoryStream memory = new MemoryStream();



                byte[] foto = memory.ToArray();

                const string string_conexao = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";

                SqlConnection dbConexao = new SqlConnection(string_conexao);


                SqlCommand cmd = new SqlCommand("Delete from ImagemFuncionario where nome = @nome", dbConexao);

                SqlParameter nome = new SqlParameter("@nome", SqlDbType.VarChar);
                SqlParameter imagem = new SqlParameter(@"foto", SqlDbType.VarBinary);

                nome.Value = txtCodigo.Text;

                imagem.Value = foto;

                cmd.Parameters.Add(nome);

                cmd.Parameters.Add(imagem);


                try
                {

                    dbConexao.Open();

                    cmd.ExecuteNonQuery();

                    txtCodigo.Text = " ";

                    pictureBox1.Image = null;

                }
                catch (Exception E)
                {

                    MessageBox.Show(E.Message);

                }
                finally
                {

                    dbConexao.Close();
                    Close();
                    Cadastro_funcionario f1 = new Cadastro_funcionario();
                    f1.Show();

                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {

            if (pictureBox1.Image == null)
            {
                MessageBox.Show("Não pode efetuar o cadastro sem Imagem", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            else if (txtNome.TextLength < 4 || txtCodigo.Text == string.Empty || !txtCpf.MaskCompleted || !txtNascimento.MaskCompleted || !txtRg.MaskCompleted || !txtTelefone.MaskCompleted || !txtCep.MaskCompleted || !txtCelular.MaskCompleted || txtEndereco.TextLength < 4 || txtBairro.TextLength < 4 || txtEstado.Text == string.Empty || txtCidade.TextLength < 4)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*)!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         
          
                else
                {



                    Regex regex = new Regex(@"[^\d]");
                    CadFuncionario fun1 = new CadFuncionario(int.Parse(txtCodigo.Text), txtNome.Text, Convert.ToDateTime(txtNascimento.Text), txtSexo.Text, txtTipoSanguineo.Text, txtEndereco.Text, txtBairro.Text, txtCidade.Text, txtEstado.Text, Convert.ToInt64(regex.Replace(txtCep.Text, "")), Convert.ToInt64(regex.Replace(txtTelefone.Text, "")), Convert.ToInt64(regex.Replace(txtCelular.Text, "")), Convert.ToInt64(regex.Replace(txtRg.Text, "")), Convert.ToInt64(regex.Replace(txtCpf.Text, "")), txtEstadoCivil.Text, txtEmail.Text, txtNaturalidade.Text, txtCor.Text, txtGrauDeInstrucao.Text, txtFormacao.Text, txtEspecialidade.Text, txtCargo.Text, txtConjuge.Text);
                    MemoryStream memory = new MemoryStream();

                    bmp.Save(memory, ImageFormat.Bmp);

                    byte[] foto = memory.ToArray();

                    const string string_conexao = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";

                    SqlConnection dbConexao = new SqlConnection(string_conexao);

                    SqlCommand cmd = new SqlCommand("insert into ImagemFuncionario (nome, foto) values (@nome,@foto)", dbConexao);

                    SqlParameter nome = new SqlParameter("@nome", SqlDbType.VarChar);
                    SqlParameter imagem = new SqlParameter(@"foto", SqlDbType.VarBinary);

                    nome.Value = txtCodigo.Text;

                    imagem.Value = foto;

                    cmd.Parameters.Add(nome);

                    cmd.Parameters.Add(imagem);


                    try
                    {

                        dbConexao.Open();

                        cmd.ExecuteNonQuery();

                        txtCodigo.Text = " ";

                        pictureBox1.Image = null;

                    }
                    catch (Exception E)
                    {

                        MessageBox.Show(E.Message);

                    }
                    finally
                    {

                        dbConexao.Close();
                        Close();
                        Cadastro_funcionario f1 = new Cadastro_funcionario();
                        f1.Show();


                    }


                }

            
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (txtNome.TextLength < 4 || txtCodigo.Text == string.Empty || !txtCpf.MaskCompleted || !txtNascimento.MaskCompleted || !txtRg.MaskCompleted || !txtTelefone.MaskCompleted || !txtCep.MaskCompleted || !txtCelular.MaskCompleted || txtEndereco.TextLength < 4 || txtBairro.TextLength < 4 || txtEstado.Text == string.Empty || txtCidade.TextLength < 4)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*)!!! para alterar os dados do funcionario", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {


                Regex regex = new Regex(@"[^\d]");
                AlterarFuncionario fun1 = new AlterarFuncionario(int.Parse(txtCodigo.Text), txtNome.Text, Convert.ToDateTime(txtNascimento.Text), txtSexo.Text, txtTipoSanguineo.Text, txtEndereco.Text, txtBairro.Text, txtCidade.Text, txtEstado.Text, Convert.ToInt64(regex.Replace(txtCep.Text, "")), Convert.ToInt64(regex.Replace(txtTelefone.Text, "")), Convert.ToInt64(regex.Replace(txtCelular.Text, "")), Convert.ToInt64(regex.Replace(txtRg.Text, "")), Convert.ToInt64(regex.Replace(txtCpf.Text, "")), txtEstadoCivil.Text, txtEmail.Text, txtNaturalidade.Text, txtCor.Text, txtGrauDeInstrucao.Text, txtFormacao.Text, txtEspecialidade.Text, txtCargo.Text, txtConjuge.Text);



                Close();
                Cadastro_funcionario f1 = new Cadastro_funcionario();
                f1.Show();




            }

            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text == string.Empty)
            {
                MessageBox.Show("por favor digitar o codigo do funcionario para verificar seus dados");
            }
            else
            {
                const string string_conexao = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";

                SqlConnection conexao = new SqlConnection(string_conexao);

                SqlCommand cmd2 = new SqlCommand("select LTRIM (nome), foto from ImagemFuncionario where LTRIM (nome) = @nome ", conexao);

                SqlParameter nomes1 = new SqlParameter("@nome", SqlDbType.VarChar);

                nomes1.Value = txtCodigo.Text;

                cmd2.Parameters.Add(nomes1);

                try
                {

                    conexao.Open();

                    SqlDataReader reader = cmd2.ExecuteReader();

                    reader.Read();

                    if (reader.HasRows)
                    {
                        txtCodigo.Text = reader[0].ToString();

                        byte[] imagem = (byte[])(reader[1]);

                        if (imagem == null)
                        {

                            pictureBox1.Image = null;

                        }
                        else
                        {
                            MemoryStream memory = new MemoryStream(imagem);

                            pictureBox1.Image = Image.FromStream(memory);
                            pictureBox1.Refresh();
                        }

                        pictureBox1.Refresh();
                    }
                    conexao.Close();

                }
                catch (Exception E)
                {

                    MessageBox.Show(E.Message);
                }


                strSql = "SELECT * FROM Cadastro_Funcionario WHERE Cod_funcionario = @Cod_funcionario ";
                Sqlcon = new SqlConnection(Strcon);
                SqlCommand cmd = new SqlCommand(strSql, Sqlcon);
                cmd.Parameters.Add("@Cod_funcionario", SqlDbType.VarChar).Value = txtCodigo.Text;

                try
                {


                    if (txtCodigo.Text == String.Empty)
                    {
                        throw new Exception("você precisa digitar o codigo do paciente!!!");
                    }
                    Sqlcon.Open();
                    SqlDataReader reader = cmd.ExecuteReader();



                    if (reader.HasRows == false)
                    {

                        throw new Exception("prontuario cadastrado com sucesso!!!");

                    }
                    reader.Read();


                    txtCodigo.Text = Convert.ToString(reader["Cod_funcionario"]);
                    txtNome.Text = Convert.ToString(reader["nome_funcionario"]);
                    txtEndereco.Text = Convert.ToString(reader["endereco"]);
                    txtCep.Text = Convert.ToString(reader["cep"]);
                    txtBairro.Text = Convert.ToString(reader["bairro"]);
                    txtCidade.Text = Convert.ToString(reader["cidade"]);
                    txtCelular.Text = Convert.ToString(reader["celular"]);
                    txtTelefone.Text = Convert.ToString(reader["telefone"]);

                    txtRg.Text = Convert.ToString(reader["rg"]);
                    txtCpf.Text = Convert.ToString(reader["cpf"]);
                    txtSexo.Text = Convert.ToString(reader["sexo"]);

                    txtNascimento.Text = Convert.ToString(reader["Nascimento"]);
                    txtTipoSanguineo.Text = Convert.ToString(reader["Tipo_Sanguinio"]);
                    txtEstado.Text = Convert.ToString(reader["estado"]);
                    txtNaturalidade.Text = Convert.ToString(reader["naturalidade"]);
                    txtCor.Text = Convert.ToString(reader["cor"]);
                    txtEstadoCivil.Text = Convert.ToString(reader["estado_civil"]);

                    txtEmail.Text = Convert.ToString(reader["email"]);
                    txtGrauDeInstrucao.Text = Convert.ToString(reader["Grau_de_instrucao"]);
                    txtFormacao.Text = Convert.ToString(reader["formacao"]);
                    txtEspecialidade.Text = Convert.ToString(reader["especialidade"]);

                    txtCargo.Text = Convert.ToString(reader["cargo"]);
                    txtConjuge.Text = Convert.ToString(reader["conjuge"]);









                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.ToString());
                }
                finally
                {
                    Sqlcon.Close();
                }
            }
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente ps = new Pesquisar_Paciente();
            ps.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico medico = new pesquisar_Medico();
            medico.Show();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 Paciente = new Form3();
            Paciente.Show();
        }

        private void especialidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade medicoEs = new pesquisaMedicoEspecialidade();
            medicoEs.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario Fun1 = new Cadastro_funcionario();
            Fun1.Show();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico cadMed = new Cadastro_medico();
            cadMed.Show();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void entrarComAdministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Administrador adm = new Administrador();
            adm.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {
            txtNome.MaxLength = 50;
        }

        private void txtEndereco_TextChanged(object sender, EventArgs e)
        {
            txtEndereco.MaxLength = 50;
        }

        private void txtBairro_TextChanged(object sender, EventArgs e)
        {
            txtBairro.MaxLength = 30;
        }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {
            txtCidade.MaxLength = 30;
        }

        private void txtNaturalidade_TextChanged(object sender, EventArgs e)
        {
            txtNaturalidade.MaxLength = 30;
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {
            txtEmail.MaxLength = 50;
        }

        private void txtFormacao_TextChanged(object sender, EventArgs e)
        {
            txtFormacao.MaxLength = 50;
        }

        private void txtEspecialidade_TextChanged(object sender, EventArgs e)
        {
            txtEspecialidade.MaxLength = 40;
        }

        private void txtConjuge_TextChanged(object sender, EventArgs e)
        {
            txtConjuge.MaxLength = 50;
        }

        private void txtNascimento_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
          
        }

        private void txtNascimento_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
               
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtNascimento.Clear();
            }
        }

        private void Cadastro_funcionario_Load(object sender, EventArgs e)
        {

        }
    }
    }
    
    

