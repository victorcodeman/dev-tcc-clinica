﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica.bd_connection
{
    public partial class Cadastro_Relatorio : Form
    {
       
        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        private string strSql1 = string.Empty;

        public Cadastro_Relatorio()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Form2 f = new Form2();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!txtDataRelatorio.MaskCompleted || txtRelatorio.Text == string.Empty || txtCodigoPaciente.Text == string.Empty || txtMedico.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*)! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                CadRelatorio c = new CadRelatorio(txtRelatorio.Text, Convert.ToDateTime(txtDataRelatorio.Text), int.Parse(txtCodigoPaciente.Text), int.Parse(txtMedico.Text), txtDadosRelatorios.Text);
                Close();
                Cadastro_Relatorio p1 = new Cadastro_Relatorio();
                p1.Show();

            }

           
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!txtDataRelatorio.MaskCompleted || txtRelatorio.Text == string.Empty || txtCodigoPaciente.Text == string.Empty || txtMedico.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios para alterar os dados do relatório(*)! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                AlterarRelatorio c1 = new AlterarRelatorio(int.Parse(txtCodigo.Text), txtRelatorio.Text, Convert.ToDateTime(txtDataRelatorio.Text), int.Parse(txtCodigoPaciente.Text), int.Parse(txtMedico.Text), txtDadosRelatorios.Text);
                Close();
                Cadastro_Relatorio p1 = new Cadastro_Relatorio();
                p1.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!txtDataRelatorio.MaskCompleted || txtRelatorio.Text == string.Empty || txtCodigoPaciente.Text == string.Empty || txtMedico.Text == string.Empty)
            {
                MessageBox.Show("por favor digite o codigo do relatorio para deleta-lo do sistema");
            }
            else
            {

                DeletarRelatorio d1 = new DeletarRelatorio(int.Parse(txtCodigo.Text), txtRelatorio.Text, Convert.ToDateTime(txtDataRelatorio.Text), int.Parse(txtCodigoPaciente.Text), int.Parse(txtMedico.Text), txtDadosRelatorios.Text);
                Close();
                Cadastro_Relatorio p1 = new Cadastro_Relatorio();
                p1.Show();
            }
        }


        private void button6_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text == string.Empty)
            {
                MessageBox.Show("por favor digitar o codigo do relatorio para verficar seus dados");
            }

            else
            {
                strSql = "SELECT * FROM Cadastro_Relatorio WHERE cod_relatorio = @cod_relatorio ";
                Sqlcon = new SqlConnection(Strcon);
                SqlCommand cmd = new SqlCommand(strSql, Sqlcon);
                cmd.Parameters.Add("@cod_relatorio", SqlDbType.VarChar).Value = txtCodigo.Text;

                try
                {


                    if (txtCodigo.Text == String.Empty)
                    {
                        throw new Exception("você precisa digitar o codigo do paciente!!!");
                    }
                    Sqlcon.Open();
                    SqlDataReader reader = cmd.ExecuteReader();



                    if (reader.HasRows == false)
                    {

                        throw new Exception("prontuario cadastrado com sucesso!!!");

                    }
                    reader.Read();


                    txtCodigo.Text = Convert.ToString(reader["cod_relatorio"]);
                    txtCodigoPaciente.Text = Convert.ToString(reader["Codigo_Paciente"]);
                    txtMedico.Text = Convert.ToString(reader["Codigo_medico"]);
                    txtRelatorio.Text = Convert.ToString(reader["tipo_relatorio"]);
                    txtDataRelatorio.Text = Convert.ToString(reader["data_Relatorio"]);
                    

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.ToString());
                }
                finally
                {
                    Sqlcon.Close();
                }
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();
                
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Paciente where nome like'" + txtpesquisarPaciente.Text + "%'",conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);
               

                dataGridView1.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Relatorio where cod_relatorio like'" + txtPesquisarRelatorio.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);
               

                dataGridView2.DataSource = tabela;

                dataGridView2.Columns[0].HeaderText = "Identificação ";
                dataGridView2.Columns[1].HeaderText = "Tipo Relatório ";
                dataGridView2.Columns[2].HeaderText = "Data Relatório ";
                dataGridView2.Columns[3].HeaderText = "Identificação Paciente ";
                dataGridView2.Columns[4].HeaderText = "Identificação Medico ";
                dataGridView2.Columns[5].HeaderText = "Relatório ";


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();
                
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Medico where nome_medico like'" + txtPesquisaMedico.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();





                instrucao.Fill(tabela);
             

                dataGridView3.DataSource = tabela;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void entrarComoAdiministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void especialidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade pes = new pesquisaMedicoEspecialidade();
            pes.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 p = new Form2();
            p.Show();
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente p1 = new Pesquisar_Paciente();
            p1.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico m1 = new pesquisar_Medico();
            m1.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 f1 = new Form3();
            f1.Show();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico m1 = new Cadastro_medico();
            m1.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario f1 = new Cadastro_funcionario();
            f1.Show();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void txtDadosRelatorios_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDataRelatorio_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtDataRelatorio.Clear();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                connection conexao = new connection();
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM prontuario where cod_prontuario Like'" + txtProntuario.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();

                instrucao.Fill(tabela);

                dataGridView4.DataSource = tabela;

                dataGridView4.Columns[0].HeaderText = "Identificação";
                dataGridView4.Columns[1].HeaderText = "Identificação Paciente ";
                dataGridView4.Columns[2].HeaderText = "Altura ";
                dataGridView4.Columns[3].HeaderText = "Peso ";
                dataGridView4.Columns[4].HeaderText = "Medicamento";
                dataGridView4.Columns[5].HeaderText = "Diagnostico ";
                dataGridView4.Columns[6].HeaderText = "Alergias ";
                dataGridView4.Columns[7].HeaderText = "Hipostese Diagnostica";
                dataGridView4.Columns[8].HeaderText = "Problemas de Saúde ";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Cadastro_Relatorio_Load(object sender, EventArgs e)
        {

        }
    }
}
