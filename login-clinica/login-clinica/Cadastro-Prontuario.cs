﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace login_clinica
{
    public partial class Cadastro_Prontuario : Form
    {

        
        public Cadastro_Prontuario()
        {
            InitializeComponent();
        }

        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        private string strSql1 = string.Empty;

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico pm1 = new pesquisar_Medico();
            pm1.Show();
        }

        private void btImagem_Click(object sender, EventArgs e)
        {

            

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtCodigo1.Text == string.Empty || txtPeso.TextLength == 0 || txtAltura.TextLength == 0)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*) para deletar os dados do prontuario", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                DeletarProntuario pron2 = new DeletarProntuario(int.Parse(txtCodProntuario.Text), float.Parse(txtAltura.Text), float.Parse(txtPeso.Text), txtMedicamento.Text, txtDiagnostico.Text, txtAlergia.Text, txtHipoteseDiagnostica.Text, txtProblemasDeSaude.Text, int.Parse(txtCodigo1.Text));
                Close();
                Cadastro_Prontuario p = new Cadastro_Prontuario();
                p.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (txtCodigo1.Text == string.Empty || txtPeso.TextLength == 0 || txtAltura.TextLength == 0)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*) ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                CadProntuario pron1 = new CadProntuario(float.Parse(txtAltura.Text), float.Parse(txtPeso.Text), txtMedicamento.Text, txtDiagnostico.Text, txtAlergia.Text, txtHipoteseDiagnostica.Text, txtProblemasDeSaude.Text, int.Parse(txtCodigo1.Text));

                Close();
                Cadastro_Prontuario p = new Cadastro_Prontuario();
                p.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtCodigo1.Text == string.Empty || txtPeso.TextLength == 0 || txtAltura.TextLength == 0)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*) para alterar os dados do prontuario", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            AlterarProntuario pron2 = new AlterarProntuario(int.Parse(txtCodProntuario.Text),float.Parse(txtAltura.Text), float.Parse(txtPeso.Text), txtMedicamento.Text, txtDiagnostico.Text, txtAlergia.Text, txtHipoteseDiagnostica.Text, txtProblemasDeSaude.Text,int.Parse(txtCodigo1.Text));
            Close();
            Cadastro_Prontuario p = new Cadastro_Prontuario();
            p.Show();
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (txtCodProntuario.Text == string.Empty)
            {
                MessageBox.Show("por favor digitar o codigo de prontuario para verificar os dados");
            }
            else
            {
                strSql1 = "SELECT * FROM prontuario WHERE cod_prontuario = @cod_prontuario";
                Sqlcon = new SqlConnection(Strcon);
                SqlCommand cmd1 = new SqlCommand(strSql1, Sqlcon);
                cmd1.Parameters.Add("@cod_prontuario", SqlDbType.VarChar).Value = txtCodProntuario.Text;


                try
                {


                    if (txtCodProntuario.Text == String.Empty)
                    {
                        throw new Exception("você precisa digitar o codigo do prontuario!!!");
                    }
                    Sqlcon.Open();
                    SqlDataReader reader = cmd1.ExecuteReader();

                    if (reader.HasRows == false)
                    {

                        throw new Exception("consulta cadastrada com sucesso!!!");

                    }

                    reader.Read();

                    txtCodProntuario.Text = Convert.ToString(reader["cod_prontuario"]);
                    txtCodigo1.Text = Convert.ToString(reader["Cod_pacient"]);
                    txtAltura.Text = Convert.ToString(reader["altura"]);
                    txtPeso.Text = Convert.ToString(reader["peso"]);
                    txtMedicamento.Text = Convert.ToString(reader["medicamento"]);
                    txtDiagnostico.Text = Convert.ToString(reader["diaginosticos"]);
                    txtAlergia.Text = Convert.ToString(reader["alergias"]);
                    txtHipoteseDiagnostica.Text = Convert.ToString(reader["hipotese_diagnostica"]);
                    txtProblemasDeSaude.Text = Convert.ToString(reader["problemas_Saude"]);






                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.ToString());
                }
                finally
                {
                    Sqlcon.Close();
                }

            }
        }
        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente pi1 = new Pesquisar_Paciente();
            pi1.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 Paciente = new Form3();
            Paciente.Show();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico m1 = new Cadastro_medico();
            m1.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario f1 = new Cadastro_funcionario();
            f1.Show();
        }

        private void especialidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade m1 = new pesquisaMedicoEspecialidade();
            m1.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void loginDeAdministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btPesquisarPaciente_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();
              
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Paciente where nome like'" + txtPesquisarPaciente.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);


                dataGridView3.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();

                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM prontuario where cod_prontuario like'" + txtPront.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);


                dataGridView1.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtPeso_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            char nome = e.KeyChar;
            if(!char.IsDigit(nome)&& nome != 8 && nome != 46)
            {

                e.Handled = true;
            }

                   
        }

        private void txtAltura_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtAltura_KeyPress(object sender, KeyPressEventArgs e)
        {
            char nome = e.KeyChar;
            if (!char.IsDigit(nome) && nome != 8 && nome != 46)
            {

                e.Handled = true;
            }
        }

        private void txtHipoteseDiagnostica_TextChanged(object sender, EventArgs e)
        {
            txtHipoteseDiagnostica.MaxLength = 150;
        }

        private void txtDiagnostico_TextChanged(object sender, EventArgs e)
        {
            txtDiagnostico.MaxLength = 150;
        }

        private void txtProblemasDeSaude_TextChanged(object sender, EventArgs e)
        {
            txtProblemasDeSaude.MaxLength = 150;
        }

        private void txtMedicamento_TextChanged(object sender, EventArgs e)
        {
            txtMedicamento.MaxLength = 150;
        }

        private void txtAlergia_TextChanged(object sender, EventArgs e)
        {
            txtAlergia.MaxLength = 150;
        }

        private void txtCodProntuario_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            txtCodProntuario.Enabled = false;
        }

        private void Cadastro_Prontuario_Load(object sender, EventArgs e)
        {

        }
    }
}
