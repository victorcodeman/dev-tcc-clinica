﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public class AlterarUsuario
    {

     
        SqlCommand cmd = new SqlCommand();
        connection dbbanco = new connection();
      
        public AlterarUsuario(int cd_usuario, int cd_perfil,String tipo_perfil, String senha_usuario, String nome_usuario, String senha2)
        {


                cmd.CommandText = "Update Usuario set cd_usuario=@cd_usuario,cd_perfil=@cd_perfil,tipo_perfil= @tipo_perfil,senha_usuario=@senha_usuario,nome_usuario=@nome_usuario where cd_usuario=@cd_usuario";

             cmd.Parameters.AddWithValue("@cd_usuario", cd_usuario);
            cmd.Parameters.AddWithValue("@cd_perfil", cd_perfil);
            cmd.Parameters.AddWithValue("@tipo_perfil", tipo_perfil);
            cmd.Parameters.AddWithValue("@senha_usuario", senha_usuario);
                cmd.Parameters.AddWithValue("@nome_usuario", nome_usuario);




            try
            {
                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Alteração Efetuada com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }
        }
        }
    }

