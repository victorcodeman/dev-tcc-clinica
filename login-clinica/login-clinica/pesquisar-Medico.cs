﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class pesquisar_Medico : Form
    {
        public pesquisar_Medico()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                connection conexao = new connection();

               
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Medico where nome_medico like'" + txtPesquisaPaciente.Text + "%'", conexao.Conectar().ConnectionString);
                DataTable tabela = new DataTable();
                instrucao.Fill(tabela);
                

                dataGridView1.DataSource = tabela;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente pa1 = new Pesquisar_Paciente();
            pa1.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico m1 = new pesquisar_Medico();
            m1.Show();
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 paciente = new Form3();
            paciente.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario funcionario = new Cadastro_funcionario();
            funcionario.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void especalidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade medico = new pesquisaMedicoEspecialidade();
            medico.Show();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void entrarLoginDeAdminstradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico m1 = new Cadastro_medico();
            m1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }
    }
}
