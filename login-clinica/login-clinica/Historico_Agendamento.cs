﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{


    public partial class Historico_Agendamento : Form
    {
        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        private string strSql1 = string.Empty;
        public Historico_Agendamento()
        {
            InitializeComponent();
        }

    
        private void btPesquisarPaciente_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();
             
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Paciente where nome like'" + txtPesquisarPaciente.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);
               

                dataGridView3.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                connection conexao = new connection();

              
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Medico where nome_medico like'" + txtListaMedico.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();





                instrucao.Fill(tabela);
               


                dataGridView2.DataSource = tabela;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                connection conexao = new connection();

              
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Historico_Agendamento where Data like'" + txtPesquisarMedico.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();



                instrucao.Fill(tabela);
               



                dataGridView1.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

           
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtCodigo1.Text == string.Empty || txtMedico.Text == string.Empty || txtTipoConsulta.Text == string.Empty || !txtData.MaskCompleted || !txtDataAgendamento.MaskCompleted || !txtDataConsulta.MaskCompleted || txtTipoAgendamento.Text == string.Empty || txtStatusAgendamento.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios para deletar (*)!!! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            String Data = txtDataConsulta.Text;
            DeletarHistoricoAgendamento del1 = new DeletarHistoricoAgendamento(int.Parse(txtCodigoConsulta.Text), txtTipoAgendamento.Text, txtTipoConsulta.Text, DateTime.Parse(txtDataConsulta.Text), Convert.ToDateTime(txtData.Text), int.Parse(txtMedico.Text), int.Parse(txtCodigo1.Text), DateTime.Parse(txtDataAgendamento.Text), txtStatusAgendamento.Text, Data);
            Close();
            Historico_Agendamento h = new Historico_Agendamento();
            h.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if(txtCodigoConsulta.Text == string.Empty)
            {
                MessageBox.Show("por favor digitar o codigo de consulta para deletar a consulta do histórico");
                    
            }


            strSql1 = "SELECT * FROM Historico_Agendamento WHERE cod_historico = @cod_historico";
            Sqlcon = new SqlConnection(Strcon);
            SqlCommand cmd1 = new SqlCommand(strSql1, Sqlcon);
            cmd1.Parameters.Add("@cod_historico", SqlDbType.VarChar).Value = txtCodigoConsulta.Text;


            try
            {


                if (txtCodigoConsulta.Text == String.Empty)
                {
                    throw new Exception("você precisa digitar o codigo da consulta!");
                }
                Sqlcon.Open();
                SqlDataReader reader = cmd1.ExecuteReader();

                if (reader.HasRows == false)
                {

                    throw new Exception("consulta cadastrada com sucesso!");

                }

                reader.Read();


                txtCodigoConsulta.Text = Convert.ToString(reader["cod_historico"]);
                txtDataConsulta.Text = Convert.ToString(reader["Data_Consulta"]);
                txtMedico.Text = Convert.ToString(reader["Medico"]);
                txtCodigo1.Text = Convert.ToString(reader["Cod_pacient"]);
                txtTipoConsulta.Text = Convert.ToString(reader["Tipo_Consulta"]);
                txtDataAgendamento.Text = Convert.ToString(reader["Data_Agendamento"]);
                txtTipoAgendamento.Text = Convert.ToString(reader["Tipo_Agendamento"]);
                txtStatusAgendamento.Text = Convert.ToString(reader["Status_Consulta"]);
                txtData.Text = Convert.ToString(reader["DataAte"]);


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                Sqlcon.Close();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form2 principal = new Form2();
            principal.Show();
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente pi1 = new Pesquisar_Paciente();
            pi1.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico pm1 = new pesquisar_Medico();
            pm1.Show();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 Paciente = new Form3();
            Paciente.Show();
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico m1 = new Cadastro_medico();
            m1.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario f1 = new Cadastro_funcionario();
            f1.Show();
        }

        private void especialidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade m1 = new pesquisaMedicoEspecialidade();
            m1.Show();
        }

        private void entrarComoAdiministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void txtPesquisarMedico_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtDataConsulta_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtDataConsulta_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
                
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtDataConsulta.Clear();
            }
        }

        private void txtData_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
                
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtData.Clear();
            }
        }

        private void txtDataAgendamento_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DateTime valor = (DateTime)e.ReturnValue;
                
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtDataAgendamento.Clear();
            }
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            if (txtCodigo1.Text == string.Empty || txtMedico.Text == string.Empty || txtTipoConsulta.Text == string.Empty || !txtData.MaskCompleted || !txtDataAgendamento.MaskCompleted || !txtDataConsulta.MaskCompleted || txtTipoAgendamento.Text == string.Empty || txtStatusAgendamento.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*)! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                String Data = txtDataConsulta.Text;
                CadHistoricoAgendamento cad1 = new CadHistoricoAgendamento(int.Parse(txtCodigoConsulta.Text), txtTipoAgendamento.Text, txtTipoConsulta.Text, Convert.ToDateTime(txtDataConsulta.Text), Convert.ToDateTime(txtData.Text), int.Parse(txtMedico.Text), int.Parse(txtCodigo1.Text), Convert.ToDateTime(txtDataAgendamento.Text), txtStatusAgendamento.Text, Data);
                Close();
                Historico_Agendamento a = new Historico_Agendamento();
                a.Show();
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {

            if (txtCodigo1.Text == string.Empty || txtMedico.Text == string.Empty || txtTipoConsulta.Text == string.Empty || !txtData.MaskCompleted || !txtDataAgendamento.MaskCompleted || !txtDataConsulta.MaskCompleted || txtTipoAgendamento.Text == string.Empty || txtStatusAgendamento.Text == string.Empty)
            {
                MessageBox.Show("Por favor Preencher todos os campos obrigatórios (*) para alterar os dados do agendamento! ", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                String Data = txtDataConsulta.Text;
                AlterarHistoricoAgendamento alter2 = new AlterarHistoricoAgendamento(int.Parse(txtCodigoConsulta.Text), txtTipoAgendamento.Text, txtTipoConsulta.Text, DateTime.Parse(txtDataConsulta.Text), DateTime.Parse(txtData.Text), int.Parse(txtMedico.Text), int.Parse(txtCodigo1.Text), DateTime.Parse(txtDataAgendamento.Text), txtStatusAgendamento.Text, Data);
                Close();
                Historico_Agendamento a = new Historico_Agendamento();
                a.Show();
            }
        }

        private void Historico_Agendamento_Load(object sender, EventArgs e)
        {

        }
    }
}

