﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace login_clinica.Controle
{
    public class Controle
    {



        public bool existe; //ver se ela existe no banco ou não //
        public String statusConexao = ""; //(2) Esstado de conexão //

        //Inicio do bloco de tartativas de validação dos dados //

        public bool acessar(String login, String senha)
        {
            login_entrar login2 = new login_entrar();
            existe = login2.verificaLogin(login, senha);

            if (!login2.statusConexao.Equals(""))
            {
                this.statusConexao = login2.statusConexao;
            }
        
        
            return existe;

        }

        public bool Administrador(String login,String senha)
        {
            login_entrar adm = new login_entrar();
            existe = adm.verificaLogin2(login, senha);

            if(!adm.statusConexao.Equals(""))
            {
                this.statusConexao = adm.statusConexao;
            }


            return existe;
        }

        public bool Médico (String login,String senha)
        {
            login_entrar med = new login_entrar();
            existe = med.verificaLogin3(login, senha);

            if (!med.statusConexao.Equals(""))
            {
                this.statusConexao = med.statusConexao;
                
            }
          
            return existe;
        }
       
       
    }
}