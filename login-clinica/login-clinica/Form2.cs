﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void btCadastrarPaciente_Click(object sender, EventArgs e)
        {
            Close();
            Form3 cad1 = new Form3();
            cad1.Show();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pacienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Form3 cad1 = new Form3();
            cad1.Show();
        }

        private void btPesquisarPaciente_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente p1 = new Pesquisar_Paciente();
            p1.Show();
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Paciente p1 = new Pesquisar_Paciente();
            p1.Show();
            
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btVerificarConsultas_Click(object sender, EventArgs e)
        {
           
        }

        private void medicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_medico medico = new Cadastro_medico();
            medico.Show();
        }

        private void btProntuario_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Prontuario pro1 = new Cadastro_Prontuario();
            pro1.Show();
        }

        private void medicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico medico = new pesquisar_Medico();
            medico.Show();
        }

        private void especialidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            pesquisaMedicoEspecialidade especialidade = new pesquisaMedicoEspecialidade();
            especialidade.Show();
        }

        private void especalidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Calendario_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void btConsultas_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void btMedico_Click(object sender, EventArgs e)
        {
            Close();
            pesquisar_Medico medico = new pesquisar_Medico();
            medico.Show();
        }

        private void funcionariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_funcionario funcionario = new Cadastro_funcionario();
            funcionario.Show();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void btConvenio_Click(object sender, EventArgs e)
        {

        }

        private void entrarComoAdiministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
            
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agenda = new Agendamento_Consulta();
            agenda.Show();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Agendamento_Consulta agendamento = new Agendamento_Consulta();
            agendamento.Show();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();
             
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Agendamento_consulta where Data like'" + txtPesquisar.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();



                instrucao.Fill(tabela);
              
               



                dataGridView1.DataSource = tabela;

                dataGridView1.Columns[0].HeaderText = "Identificação";
                dataGridView1.Columns[1].HeaderText = "Tipo de Agendamento ";
                dataGridView1.Columns[2].HeaderText = "Data de Consulta ";
                dataGridView1.Columns[3].HeaderText = "Data final ";
                dataGridView1.Columns[4].HeaderText = "";
                dataGridView1.Columns[5].HeaderText = "";
                dataGridView1.Columns[6].HeaderText = "";
                dataGridView1.Columns[7].HeaderText = "";
                dataGridView1.Columns[8].HeaderText = "";
                dataGridView1.Columns[9].HeaderText = "";


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btMedicamentos_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Historico_Agendamento h1 = new Historico_Agendamento();
            h1.Show();
        }

        private void btRelatorioOperacao_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void btRelatorio_Click(object sender, EventArgs e)
        {
            Close();
            Cadastro_Relatorio c1 = new Cadastro_Relatorio();
            c1.Show();
        }

        private void btBancoDeSangue_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            Pesquisar_Funcionario pfun1 = new Pesquisar_Funcionario();
            pfun1.Show();
        }

        private void btBackup_Click(object sender, EventArgs e)
        {
            Close();
            Backup bac = new Backup();
            bac.Show();
        }

        private void btAjuda_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void apostilaDeAjudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Apostila apos = new Apostila();
            apos.Show();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            
            e.Graphics.DrawImage(bmp, -180, -60);
        }
        Bitmap bmp;
        private void btImprimir_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            bmp = new Bitmap(this.Size.Width, this.Size.Height, g);
            Graphics mg = Graphics.FromImage(bmp);
            mg.CopyFromScreen(this.Location.X, this.Location.Y, -180, -60, this.Size);
            printPreviewDialog1.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            Login_Medico m = new Login_Medico();
            m.Show();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Form1 P1= new Form1();
            P1.Show();
            
        }
    }
}
