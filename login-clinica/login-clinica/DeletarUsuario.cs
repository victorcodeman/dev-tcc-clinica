﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public class DeletarUsuario
    {
        SqlCommand cmd = new SqlCommand();  // comando sql
        connection dbbanco = new connection();
        public DeletarUsuario(int cd_usuario, int cd_perfil,String tipo_perfil, String senha_usuario, String nome_usuario, String senha2)
        {


            cmd.CommandText = "Delete from Usuario where cd_usuario=@cd_usuario";

            cmd.Parameters.AddWithValue("@cd_usuario", cd_usuario);
            cmd.Parameters.AddWithValue("@cd_perfil", cd_perfil);
            cmd.Parameters.AddWithValue("@tipo_perfil", tipo_perfil);
            cmd.Parameters.AddWithValue("@senha_usuario", senha_usuario);
            cmd.Parameters.AddWithValue("@nome_usuario", nome_usuario);

            try
            {

                cmd.Connection = dbbanco.Conectar();
                cmd.ExecuteNonQuery();

                MessageBox.Show("Usuario deletado com Sucesso!!!!!!");



            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dbbanco.Desconectar();


            }
        }
    }
        }
