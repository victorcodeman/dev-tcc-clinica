﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using login_clinica.bd_connection;

namespace login_clinica
{
    public class CadUsuario
    {
        public bool existe = false;
       
        SqlCommand cmd = new SqlCommand();
        connection dbbanco = new connection();
        

        public CadUsuario(int cd_usuario,int cd_perfil,String tipo_perfil, String senha_usuario,String nome_usuario,String senha2)
        {

           
            if(senha_usuario.Equals(senha2)){

                cmd.CommandText = "Insert into Usuario values (@cd_usuario,@cd_perfil,@tipo_perfil,@senha_usuario,@nome_usuario)";

                cmd.Parameters.AddWithValue("@cd_usuario", cd_usuario);
                cmd.Parameters.AddWithValue("@cd_perfil", cd_perfil);
                cmd.Parameters.AddWithValue("@tipo_perfil", tipo_perfil);
                cmd.Parameters.AddWithValue("@senha_usuario", senha_usuario);
                cmd.Parameters.AddWithValue("@nome_usuario", nome_usuario);
            
                

                try
                {

                    cmd.Connection = dbbanco.Conectar();
                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Cadastro Efetuado com Sucesso!!!!!!");



                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    dbbanco.Desconectar();


                }

            }


        }
    }
}
