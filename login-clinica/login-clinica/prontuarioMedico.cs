﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class prontuarioMedico : Form
    {
        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        private string strSql1 = string.Empty;
        public prontuarioMedico()
        {
            InitializeComponent();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            prontuarioMedico m = new prontuarioMedico();
            m.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();

                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM prontuario where cod_prontuario like'" + txtPront.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);


                dataGridView1.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (txtCodProntuario.Text == string.Empty)
            {
                MessageBox.Show("digitar o codigo do prontuario para verificar os dados do prontuario ");
            }
            else
            {
                strSql1 = "SELECT * FROM prontuario WHERE cod_prontuario = @cod_prontuario";
                Sqlcon = new SqlConnection(Strcon);
                SqlCommand cmd1 = new SqlCommand(strSql1, Sqlcon);
                cmd1.Parameters.Add("@cod_prontuario", SqlDbType.VarChar).Value = txtCodProntuario.Text;


                try
                {


                    if (txtCodProntuario.Text == String.Empty)
                    {
                        throw new Exception("você precisa digitar o codigo do prontuario!");
                    }
                    Sqlcon.Open();
                    SqlDataReader reader = cmd1.ExecuteReader();

                    if (reader.HasRows == false)
                    {

                        throw new Exception("consulta cadastrada com sucesso!");

                    }

                    reader.Read();

                    txtCodProntuario.Text = Convert.ToString(reader["cod_prontuario"]);
                    txtCodigo1.Text = Convert.ToString(reader["Cod_pacient"]);
                    txtAltura.Text = Convert.ToString(reader["altura"]);
                    txtPeso.Text = Convert.ToString(reader["peso"]);
                    txtMedicamento.Text = Convert.ToString(reader["medicamento"]);
                    txtDiagnostico.Text = Convert.ToString(reader["diaginosticos"]);
                    txtAlergia.Text = Convert.ToString(reader["alergias"]);
                    txtHipoteseDiagnostica.Text = Convert.ToString(reader["hipotese_diagnostica"]);
                    txtProblemasDeSaude.Text = Convert.ToString(reader["problemas_Saude"]);






                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.ToString());
                }
                finally
                {
                    Sqlcon.Close();
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Close();
            RelatorioMedico r = new RelatorioMedico();
            r.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            HistoricoMedicoAgenda m = new HistoricoMedicoAgenda();
            m.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtCodProntuario.Text == string.Empty || txtCodigo1.Text == string.Empty || txtPeso.TextLength == 0 || txtAltura.TextLength == 0)
            {
                MessageBox.Show("Digite todos os campos obrigatórios para alterar os dados");
            }
            else
            {
                AlterarProntuario pron2 = new AlterarProntuario(int.Parse(txtCodProntuario.Text), float.Parse(txtAltura.Text), float.Parse(txtPeso.Text), txtMedicamento.Text, txtDiagnostico.Text, txtAlergia.Text, txtHipoteseDiagnostica.Text, txtProblemasDeSaude.Text, int.Parse(txtCodigo1.Text));
                Close();
             prontuarioMedico p = new prontuarioMedico();
                p.Show();
            }

        }

        private void txtHipoteseDiagnostica_TextChanged(object sender, EventArgs e)
        {
            txtHipoteseDiagnostica.MaxLength = 150;
        }

        private void txtDiagnostico_TextChanged(object sender, EventArgs e)
        {
            txtDiagnostico.MaxLength = 150;
        }

        private void txtProblemasDeSaude_TextChanged(object sender, EventArgs e)
        {
            txtProblemasDeSaude.MaxLength = 150;
        }

        private void txtMedicamento_TextChanged(object sender, EventArgs e)
        {
            txtMedicamento.MaxLength = 150;
        }

        private void txtAlergia_TextChanged(object sender, EventArgs e)
        {
            txtAlergia.MaxLength = 150;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                connection conexao = new connection();
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Paciente where Cod_paciente Like '" + txtMedico.Text + "%'",conexao.Conectar().ConnectionString);
                DataTable tabela = new DataTable();
                instrucao.Fill(tabela);

                dataGridView2.DataSource = (tabela);

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtPeso_TextChanged(object sender, EventArgs e)
        {


           
        }

        private void txtAltura_TextChanged(object sender, EventArgs e)
        {

        }
 private void txtAltura_KeyPress(object sender, KeyPressEventArgs e)
        {
            char nome = e.KeyChar;
            if (!char.IsDigit(nome) && nome != 8 && nome != 46)
            {

                e.Handled = true;
            }
        }

        private void txtAltura_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            char nome = e.KeyChar;
            if (!char.IsDigit(nome) && nome != 8 && nome != 46)
            {

                e.Handled = true;
            }
        }

        private void txtPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            char nome = e.KeyChar;
            if (!char.IsDigit(nome) && nome != 8 && nome != 46)
            {

                e.Handled = true;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
            Login_Medico m = new Login_Medico();
            m.Show();
        }

        private void prontuarioMedico_Load(object sender, EventArgs e)
        {

        }
    }
}

