﻿namespace login_clinica
{
    partial class Cadastro_Prontuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cadastro_Prontuario));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.btSair1 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btSair = new System.Windows.Forms.Button();
            this.btRelatorio = new System.Windows.Forms.Button();
            this.btAdm = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.principalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.telaPrincipalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesquisarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacienteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.medicoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.médicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.especialidadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administradorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginDeAdministradorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agendamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.Pai = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHipoteseDiagnostica = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDiagnostico = new System.Windows.Forms.TextBox();
            this.txtProblemasDeSaude = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAlergias = new System.Windows.Forms.Label();
            this.txtAltura = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMedicamento = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAlergia = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.txtPesquisarPaciente = new System.Windows.Forms.TextBox();
            this.btPesquisarPaciente = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.txtCodigo1 = new System.Windows.Forms.MaskedTextBox();
            this.txtCodProntuario = new System.Windows.Forms.MaskedTextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.txtPront = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Location = new System.Drawing.Point(524, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(368, 85);
            this.panel1.TabIndex = 120;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(938, 583);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(57, 66);
            this.button4.TabIndex = 119;
            this.button4.Text = "Excluir";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel3
            // 
            this.panel3.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.button8);
            this.panel3.Controls.Add(this.btSair1);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(-2, 83);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(323, 608);
            this.panel3.TabIndex = 118;
            // 
            // button8
            // 
            this.button8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button8.BackColor = System.Drawing.Color.Transparent;
            this.button8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button8.BackgroundImage")));
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.ForeColor = System.Drawing.Color.Transparent;
            this.button8.Location = new System.Drawing.Point(30, 24);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(268, 68);
            this.button8.TabIndex = 138;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btSair1
            // 
            this.btSair1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSair1.BackColor = System.Drawing.Color.Transparent;
            this.btSair1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btSair1.BackgroundImage")));
            this.btSair1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSair1.ForeColor = System.Drawing.Color.Transparent;
            this.btSair1.Location = new System.Drawing.Point(30, 132);
            this.btSair1.Margin = new System.Windows.Forms.Padding(0);
            this.btSair1.Name = "btSair1";
            this.btSair1.Size = new System.Drawing.Size(268, 68);
            this.btSair1.TabIndex = 9;
            this.btSair1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btSair1.UseVisualStyleBackColor = false;
            this.btSair1.Click += new System.EventHandler(this.btSair1_Click);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btSair);
            this.panel5.Controls.Add(this.btRelatorio);
            this.panel5.Controls.Add(this.btAdm);
            this.panel5.Location = new System.Drawing.Point(-2, 25);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1134, 58);
            this.panel5.TabIndex = 117;
            // 
            // btSair
            // 
            this.btSair.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btSair.BackgroundImage")));
            this.btSair.Location = new System.Drawing.Point(143, 3);
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(62, 52);
            this.btSair.TabIndex = 16;
            this.btSair.UseVisualStyleBackColor = true;
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // btRelatorio
            // 
            this.btRelatorio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btRelatorio.BackgroundImage")));
            this.btRelatorio.Location = new System.Drawing.Point(75, 2);
            this.btRelatorio.Name = "btRelatorio";
            this.btRelatorio.Size = new System.Drawing.Size(62, 52);
            this.btRelatorio.TabIndex = 10;
            this.btRelatorio.UseVisualStyleBackColor = true;
            this.btRelatorio.Click += new System.EventHandler(this.btRelatorio_Click);
            // 
            // btAdm
            // 
            this.btAdm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btAdm.BackgroundImage")));
            this.btAdm.Location = new System.Drawing.Point(5, 2);
            this.btAdm.Name = "btAdm";
            this.btAdm.Size = new System.Drawing.Size(62, 52);
            this.btAdm.TabIndex = 3;
            this.btAdm.UseVisualStyleBackColor = true;
            this.btAdm.Click += new System.EventHandler(this.btAdm_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.principalToolStripMenuItem,
            this.pesquisarToolStripMenuItem,
            this.cadastroToolStripMenuItem,
            this.médicoToolStripMenuItem,
            this.administradorToolStripMenuItem,
            this.agendamentoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1130, 24);
            this.menuStrip1.TabIndex = 116;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // principalToolStripMenuItem
            // 
            this.principalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.telaPrincipalToolStripMenuItem});
            this.principalToolStripMenuItem.Name = "principalToolStripMenuItem";
            this.principalToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.principalToolStripMenuItem.Text = "Inicial";
            // 
            // telaPrincipalToolStripMenuItem
            // 
            this.telaPrincipalToolStripMenuItem.Name = "telaPrincipalToolStripMenuItem";
            this.telaPrincipalToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.telaPrincipalToolStripMenuItem.Text = "Tela Principal";
            this.telaPrincipalToolStripMenuItem.Click += new System.EventHandler(this.telaPrincipalToolStripMenuItem_Click);
            // 
            // pesquisarToolStripMenuItem
            // 
            this.pesquisarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pacienteToolStripMenuItem,
            this.medicoToolStripMenuItem,
            this.funcionariosToolStripMenuItem});
            this.pesquisarToolStripMenuItem.Name = "pesquisarToolStripMenuItem";
            this.pesquisarToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.pesquisarToolStripMenuItem.Text = "Pesquisar";
            // 
            // pacienteToolStripMenuItem
            // 
            this.pacienteToolStripMenuItem.Name = "pacienteToolStripMenuItem";
            this.pacienteToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.pacienteToolStripMenuItem.Text = "Paciente";
            this.pacienteToolStripMenuItem.Click += new System.EventHandler(this.pacienteToolStripMenuItem_Click);
            // 
            // medicoToolStripMenuItem
            // 
            this.medicoToolStripMenuItem.Name = "medicoToolStripMenuItem";
            this.medicoToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.medicoToolStripMenuItem.Text = "Medico";
            this.medicoToolStripMenuItem.Click += new System.EventHandler(this.medicoToolStripMenuItem_Click);
            // 
            // funcionariosToolStripMenuItem
            // 
            this.funcionariosToolStripMenuItem.Name = "funcionariosToolStripMenuItem";
            this.funcionariosToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.funcionariosToolStripMenuItem.Text = "Funcionarios";
            this.funcionariosToolStripMenuItem.Click += new System.EventHandler(this.funcionariosToolStripMenuItem_Click);
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pacienteToolStripMenuItem1,
            this.medicoToolStripMenuItem1,
            this.funcionariosToolStripMenuItem1});
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            // 
            // pacienteToolStripMenuItem1
            // 
            this.pacienteToolStripMenuItem1.Name = "pacienteToolStripMenuItem1";
            this.pacienteToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.pacienteToolStripMenuItem1.Text = "Paciente";
            this.pacienteToolStripMenuItem1.Click += new System.EventHandler(this.pacienteToolStripMenuItem1_Click);
            // 
            // medicoToolStripMenuItem1
            // 
            this.medicoToolStripMenuItem1.Name = "medicoToolStripMenuItem1";
            this.medicoToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.medicoToolStripMenuItem1.Text = "Médico";
            this.medicoToolStripMenuItem1.Click += new System.EventHandler(this.medicoToolStripMenuItem1_Click);
            // 
            // funcionariosToolStripMenuItem1
            // 
            this.funcionariosToolStripMenuItem1.Name = "funcionariosToolStripMenuItem1";
            this.funcionariosToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.funcionariosToolStripMenuItem1.Text = "Funcionarios";
            this.funcionariosToolStripMenuItem1.Click += new System.EventHandler(this.funcionariosToolStripMenuItem1_Click);
            // 
            // médicoToolStripMenuItem
            // 
            this.médicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.especialidadeToolStripMenuItem});
            this.médicoToolStripMenuItem.Name = "médicoToolStripMenuItem";
            this.médicoToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.médicoToolStripMenuItem.Text = "Médico";
            // 
            // especialidadeToolStripMenuItem
            // 
            this.especialidadeToolStripMenuItem.Name = "especialidadeToolStripMenuItem";
            this.especialidadeToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.especialidadeToolStripMenuItem.Text = "Pesquisar por Especialidade";
            this.especialidadeToolStripMenuItem.Click += new System.EventHandler(this.especialidadeToolStripMenuItem_Click);
            // 
            // administradorToolStripMenuItem
            // 
            this.administradorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginDeAdministradorToolStripMenuItem});
            this.administradorToolStripMenuItem.Name = "administradorToolStripMenuItem";
            this.administradorToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.administradorToolStripMenuItem.Text = "Administrador";
            // 
            // loginDeAdministradorToolStripMenuItem
            // 
            this.loginDeAdministradorToolStripMenuItem.Name = "loginDeAdministradorToolStripMenuItem";
            this.loginDeAdministradorToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.loginDeAdministradorToolStripMenuItem.Text = "Login de Administrador";
            this.loginDeAdministradorToolStripMenuItem.Click += new System.EventHandler(this.loginDeAdministradorToolStripMenuItem_Click);
            // 
            // agendamentoToolStripMenuItem
            // 
            this.agendamentoToolStripMenuItem.Name = "agendamentoToolStripMenuItem";
            this.agendamentoToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.agendamentoToolStripMenuItem.Text = "Agendamento";
            this.agendamentoToolStripMenuItem.Click += new System.EventHandler(this.agendamentoToolStripMenuItem_Click);
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(699, 408);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(54, 20);
            this.txtPeso.TabIndex = 96;
            this.txtPeso.TextChanged += new System.EventHandler(this.txtPeso_TextChanged);
            this.txtPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeso_KeyPress);
            // 
            // Pai
            // 
            this.Pai.AutoSize = true;
            this.Pai.Location = new System.Drawing.Point(334, 473);
            this.Pai.Name = "Pai";
            this.Pai.Size = new System.Drawing.Size(66, 13);
            this.Pai.TabIndex = 85;
            this.Pai.Text = "Diagnostico:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(332, 441);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 13);
            this.label8.TabIndex = 83;
            this.label8.Text = "hipotese_diagnostica:";
            // 
            // txtHipoteseDiagnostica
            // 
            this.txtHipoteseDiagnostica.Location = new System.Drawing.Point(457, 438);
            this.txtHipoteseDiagnostica.Name = "txtHipoteseDiagnostica";
            this.txtHipoteseDiagnostica.Size = new System.Drawing.Size(648, 20);
            this.txtHipoteseDiagnostica.TabIndex = 82;
            this.txtHipoteseDiagnostica.TextChanged += new System.EventHandler(this.txtHipoteseDiagnostica_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(650, 411);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 81;
            this.label7.Text = "* Peso:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1070, 583);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 67);
            this.button1.TabIndex = 65;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtDiagnostico
            // 
            this.txtDiagnostico.Location = new System.Drawing.Point(457, 469);
            this.txtDiagnostico.Name = "txtDiagnostico";
            this.txtDiagnostico.Size = new System.Drawing.Size(648, 20);
            this.txtDiagnostico.TabIndex = 84;
            this.txtDiagnostico.TextChanged += new System.EventHandler(this.txtDiagnostico_TextChanged);
            // 
            // txtProblemasDeSaude
            // 
            this.txtProblemasDeSaude.Location = new System.Drawing.Point(457, 499);
            this.txtProblemasDeSaude.Name = "txtProblemasDeSaude";
            this.txtProblemasDeSaude.Size = new System.Drawing.Size(282, 20);
            this.txtProblemasDeSaude.TabIndex = 86;
            this.txtProblemasDeSaude.TextChanged += new System.EventHandler(this.txtProblemasDeSaude_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.Location = new System.Drawing.Point(566, 341);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(350, 61);
            this.panel2.TabIndex = 122;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(332, 411);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 123;
            this.label6.Text = "Codigo do Prontuário:";
            // 
            // txtAlergias
            // 
            this.txtAlergias.AutoSize = true;
            this.txtAlergias.Location = new System.Drawing.Point(334, 504);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.Size = new System.Drawing.Size(108, 13);
            this.txtAlergias.TabIndex = 125;
            this.txtAlergias.Text = "Problemas de Saúde:";
            // 
            // txtAltura
            // 
            this.txtAltura.Location = new System.Drawing.Point(582, 407);
            this.txtAltura.Name = "txtAltura";
            this.txtAltura.Size = new System.Drawing.Size(50, 20);
            this.txtAltura.TabIndex = 128;
            this.txtAltura.TextChanged += new System.EventHandler(this.txtAltura_TextChanged);
            this.txtAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAltura_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(539, 411);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 127;
            this.label11.Text = "* Altura:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(759, 537);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 130;
            this.label10.Text = "Alergias:";
            // 
            // txtMedicamento
            // 
            this.txtMedicamento.Location = new System.Drawing.Point(830, 500);
            this.txtMedicamento.Name = "txtMedicamento";
            this.txtMedicamento.Size = new System.Drawing.Size(275, 20);
            this.txtMedicamento.TabIndex = 129;
            this.txtMedicamento.TextChanged += new System.EventHandler(this.txtMedicamento_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1001, 583);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(56, 66);
            this.button2.TabIndex = 131;
            this.button2.Text = "Alterar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1037, 656);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(87, 31);
            this.button3.TabIndex = 132;
            this.button3.Text = "Sair";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(745, 502);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 134;
            this.label12.Text = "Medicamentos:";
            // 
            // txtAlergia
            // 
            this.txtAlergia.Location = new System.Drawing.Point(830, 533);
            this.txtAlergia.Name = "txtAlergia";
            this.txtAlergia.Size = new System.Drawing.Size(275, 20);
            this.txtAlergia.TabIndex = 133;
            this.txtAlergia.TextChanged += new System.EventHandler(this.txtAlergia_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(759, 411);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(110, 13);
            this.label16.TabIndex = 135;
            this.label16.Text = "* Codigo do Paciente:";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(938, 656);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(93, 31);
            this.button5.TabIndex = 137;
            this.button5.Text = "Verificar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtPesquisarPaciente
            // 
            this.txtPesquisarPaciente.Location = new System.Drawing.Point(357, 172);
            this.txtPesquisarPaciente.Name = "txtPesquisarPaciente";
            this.txtPesquisarPaciente.Size = new System.Drawing.Size(611, 20);
            this.txtPesquisarPaciente.TabIndex = 202;
            // 
            // btPesquisarPaciente
            // 
            this.btPesquisarPaciente.Location = new System.Drawing.Point(974, 171);
            this.btPesquisarPaciente.Name = "btPesquisarPaciente";
            this.btPesquisarPaciente.Size = new System.Drawing.Size(124, 24);
            this.btPesquisarPaciente.TabIndex = 201;
            this.btPesquisarPaciente.Text = "Pesquisar Pacientes";
            this.btPesquisarPaciente.UseVisualStyleBackColor = true;
            this.btPesquisarPaciente.Click += new System.EventHandler(this.btPesquisarPaciente_Click);
            // 
            // dataGridView3
            // 
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(358, 198);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(740, 137);
            this.dataGridView3.TabIndex = 200;
            // 
            // txtCodigo1
            // 
            this.txtCodigo1.Location = new System.Drawing.Point(889, 408);
            this.txtCodigo1.Mask = "0000000";
            this.txtCodigo1.Name = "txtCodigo1";
            this.txtCodigo1.Size = new System.Drawing.Size(54, 20);
            this.txtCodigo1.TabIndex = 203;
            // 
            // txtCodProntuario
            // 
            this.txtCodProntuario.Location = new System.Drawing.Point(467, 407);
            this.txtCodProntuario.Mask = "0000000";
            this.txtCodProntuario.Name = "txtCodProntuario";
            this.txtCodProntuario.Size = new System.Drawing.Size(51, 20);
            this.txtCodProntuario.TabIndex = 204;
            this.txtCodProntuario.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtCodProntuario_MaskInputRejected);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(624, 522);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(117, 28);
            this.button6.TabIndex = 205;
            this.button6.Text = "Pesquisar Prontuarios";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // txtPront
            // 
            this.txtPront.Location = new System.Drawing.Point(337, 525);
            this.txtPront.Name = "txtPront";
            this.txtPront.Size = new System.Drawing.Size(281, 20);
            this.txtPront.TabIndex = 206;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(335, 556);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(597, 93);
            this.dataGridView1.TabIndex = 207;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(334, 663);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 16);
            this.label1.TabIndex = 208;
            this.label1.Text = "(*) CAMPOS OBRIGATÓRIOS !!";
            // 
            // Cadastro_Prontuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 688);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtPront);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.txtCodProntuario);
            this.Controls.Add(this.txtCodigo1);
            this.Controls.Add(this.txtPesquisarPaciente);
            this.Controls.Add(this.btPesquisarPaciente);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtAlergia);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtMedicamento);
            this.Controls.Add(this.txtAltura);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtAlergias);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.txtProblemasDeSaude);
            this.Controls.Add(this.Pai);
            this.Controls.Add(this.txtDiagnostico);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtHipoteseDiagnostica);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Cadastro_Prontuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro_Prontuario";
            this.Load += new System.EventHandler(this.Cadastro_Prontuario_Load);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btSair1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btSair;
        private System.Windows.Forms.Button btRelatorio;
        private System.Windows.Forms.Button btAdm;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem principalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem telaPrincipalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesquisarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funcionariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacienteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem medicoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem funcionariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem médicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem especialidadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administradorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agendamentoToolStripMenuItem;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label Pai;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtHipoteseDiagnostica;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtDiagnostico;
        private System.Windows.Forms.TextBox txtProblemasDeSaude;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label txtAlergias;
        private System.Windows.Forms.TextBox txtAltura;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMedicamento;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAlergia;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ToolStripMenuItem loginDeAdministradorToolStripMenuItem;
        private System.Windows.Forms.TextBox txtPesquisarPaciente;
        private System.Windows.Forms.Button btPesquisarPaciente;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.MaskedTextBox txtCodigo1;
        private System.Windows.Forms.MaskedTextBox txtCodProntuario;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtPront;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
    }
}