﻿using login_clinica.bd_connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_clinica
{
    public partial class RelatorioMedico : Form
    {
        SqlConnection Sqlcon = null;
        private string Strcon = @"Data Source=.\SQLExpress;Initial Catalog=Clinica;Integrated Security=True";
        private string strSql = string.Empty;
        private string strSql1 = string.Empty;
        public RelatorioMedico()
        {
            InitializeComponent();
        }

        private void btSair1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btAdm_Click(object sender, EventArgs e)
        {
            Close();
            Login l1 = new Login();
            l1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            prontuarioMedico m = new prontuarioMedico();
            m.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtCodigo == null)
            {
                MessageBox.Show("Digite o codigo de relatório para verificar dados");
            }
            else {

                strSql = "SELECT * FROM Cadastro_Relatorio WHERE cod_relatorio = @cod_relatorio ";
                Sqlcon = new SqlConnection(Strcon);
                SqlCommand cmd = new SqlCommand(strSql, Sqlcon);
                cmd.Parameters.Add("@cod_relatorio", SqlDbType.VarChar).Value = txtCodigo.Text;

                try
                {


                    if (txtCodigo.Text == String.Empty)
                    {
                        throw new Exception("você precisa digitar o codigo do paciente!!!");
                    }
                    Sqlcon.Open();
                    SqlDataReader reader = cmd.ExecuteReader();



                    if (reader.HasRows == false)
                    {

                        throw new Exception("prontuario cadastrado com sucesso!!!");

                    }
                    reader.Read();


                    txtCodigo.Text = Convert.ToString(reader["cod_relatorio"]);
                    txtCodigoPaciente.Text = Convert.ToString(reader["Codigo_Paciente"]);
                    txtMedico.Text = Convert.ToString(reader["Codigo_medico"]);
                    txtRelatorio.Text = Convert.ToString(reader["tipo_relatorio"]);
                    txtDadosRelatorios.Text = Convert.ToString(reader["relatorio"]);
                    txtDataRelatorio.Text = Convert.ToString(reader["data_Relatorio"]);













                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.ToString());
                }
                finally
                {
                    Sqlcon.Close();
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {

                connection conexao = new connection();
                SqlDataAdapter instrucao = new SqlDataAdapter("SELECT * FROM Cadastro_Relatorio where cod_relatorio like'" + txtPesquisarRelatorio.Text + "%'", conexao.Conectar().ConnectionString);

                DataTable tabela = new DataTable();


                instrucao.Fill(tabela);


                dataGridView2.DataSource = tabela;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Close();
            RelatorioMedico r = new RelatorioMedico();
            r.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Close();
            HistoricoMedicoAgenda m = new HistoricoMedicoAgenda();
            m.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
            Login_Medico m = new Login_Medico();
            m.Show();
        }

        private void txtDataRelatorio_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if(e.ReturnValue != null){

                DateTime valor = (DateTime)e.ReturnValue;
            }
            else
            {
                MessageBox.Show("Data inválida");
                txtDataRelatorio.Clear();
            }
           
        }

        private void RelatorioMedico_Load(object sender, EventArgs e)
        {

        }
    }
}
