﻿namespace login_clinica
{
    partial class prontuarioMedico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(prontuarioMedico));
            this.panel5 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btAdm = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.btSair1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtPront = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAlergias = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Pai = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAlergia = new System.Windows.Forms.TextBox();
            this.txtMedicamento = new System.Windows.Forms.TextBox();
            this.txtProblemasDeSaude = new System.Windows.Forms.TextBox();
            this.txtDiagnostico = new System.Windows.Forms.TextBox();
            this.txtHipoteseDiagnostica = new System.Windows.Forms.TextBox();
            this.txtAltura = new System.Windows.Forms.TextBox();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.txtCodProntuario = new System.Windows.Forms.MaskedTextBox();
            this.txtCodigo1 = new System.Windows.Forms.MaskedTextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.txtMedico = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.button5);
            this.panel5.Controls.Add(this.button6);
            this.panel5.Controls.Add(this.btAdm);
            this.panel5.Location = new System.Drawing.Point(-3, -1);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1140, 58);
            this.panel5.TabIndex = 222;
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.Location = new System.Drawing.Point(141, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 52);
            this.button1.TabIndex = 13;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button5
            // 
            this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
            this.button5.Location = new System.Drawing.Point(209, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(62, 52);
            this.button5.TabIndex = 12;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button6.BackgroundImage")));
            this.button6.Location = new System.Drawing.Point(73, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(62, 52);
            this.button6.TabIndex = 10;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btAdm
            // 
            this.btAdm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btAdm.BackgroundImage")));
            this.btAdm.Location = new System.Drawing.Point(5, 2);
            this.btAdm.Name = "btAdm";
            this.btAdm.Size = new System.Drawing.Size(62, 52);
            this.btAdm.TabIndex = 3;
            this.btAdm.UseVisualStyleBackColor = true;
            this.btAdm.Click += new System.EventHandler(this.btAdm_Click);
            // 
            // panel3
            // 
            this.panel3.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.button8);
            this.panel3.Controls.Add(this.btSair1);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(-3, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(323, 641);
            this.panel3.TabIndex = 221;
            // 
            // button8
            // 
            this.button8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button8.BackColor = System.Drawing.Color.Transparent;
            this.button8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button8.BackgroundImage")));
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.ForeColor = System.Drawing.Color.Transparent;
            this.button8.Location = new System.Drawing.Point(24, 20);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(268, 68);
            this.button8.TabIndex = 44;
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btSair1
            // 
            this.btSair1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSair1.BackColor = System.Drawing.Color.Transparent;
            this.btSair1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btSair1.BackgroundImage")));
            this.btSair1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSair1.ForeColor = System.Drawing.Color.Transparent;
            this.btSair1.Location = new System.Drawing.Point(24, 106);
            this.btSair1.Margin = new System.Windows.Forms.Padding(0);
            this.btSair1.Name = "btSair1";
            this.btSair1.Size = new System.Drawing.Size(268, 68);
            this.btSair1.TabIndex = 9;
            this.btSair1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btSair1.UseVisualStyleBackColor = false;
            this.btSair1.Click += new System.EventHandler(this.btSair1_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Location = new System.Drawing.Point(549, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(368, 85);
            this.panel1.TabIndex = 223;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(362, 181);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(465, 250);
            this.dataGridView1.TabIndex = 211;
            // 
            // txtPront
            // 
            this.txtPront.Location = new System.Drawing.Point(362, 155);
            this.txtPront.Name = "txtPront";
            this.txtPront.Size = new System.Drawing.Size(344, 20);
            this.txtPront.TabIndex = 210;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(712, 150);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 28);
            this.button2.TabIndex = 209;
            this.button2.Text = "Pesquisar Prontuarios";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(987, 581);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(122, 50);
            this.button3.TabIndex = 208;
            this.button3.Text = "Verificar Prontuario";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(818, 452);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(110, 13);
            this.label16.TabIndex = 232;
            this.label16.Text = "* Codigo do Paciente:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(359, 597);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 231;
            this.label12.Text = "Medicamentos:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(359, 629);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 230;
            this.label10.Text = "Alergias:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(598, 451);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 229;
            this.label11.Text = "* Altura:";
            // 
            // txtAlergias
            // 
            this.txtAlergias.AutoSize = true;
            this.txtAlergias.Location = new System.Drawing.Point(359, 559);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.Size = new System.Drawing.Size(108, 13);
            this.txtAlergias.TabIndex = 228;
            this.txtAlergias.Text = "Problemas de Saúde:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(355, 451);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 227;
            this.label6.Text = "Codigo do Prontuário:";
            // 
            // Pai
            // 
            this.Pai.AutoSize = true;
            this.Pai.Location = new System.Drawing.Point(359, 522);
            this.Pai.Name = "Pai";
            this.Pai.Size = new System.Drawing.Size(66, 13);
            this.Pai.TabIndex = 226;
            this.Pai.Text = "Diagnostico:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(359, 487);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 13);
            this.label8.TabIndex = 225;
            this.label8.Text = "hipotese_diagnostica:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(712, 451);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 224;
            this.label7.Text = "* Peso:";
            // 
            // txtAlergia
            // 
            this.txtAlergia.Location = new System.Drawing.Point(495, 626);
            this.txtAlergia.Name = "txtAlergia";
            this.txtAlergia.Size = new System.Drawing.Size(345, 20);
            this.txtAlergia.TabIndex = 237;
            this.txtAlergia.TextChanged += new System.EventHandler(this.txtAlergia_TextChanged);
            // 
            // txtMedicamento
            // 
            this.txtMedicamento.Location = new System.Drawing.Point(495, 592);
            this.txtMedicamento.Name = "txtMedicamento";
            this.txtMedicamento.Size = new System.Drawing.Size(345, 20);
            this.txtMedicamento.TabIndex = 236;
            this.txtMedicamento.TextChanged += new System.EventHandler(this.txtMedicamento_TextChanged);
            // 
            // txtProblemasDeSaude
            // 
            this.txtProblemasDeSaude.Location = new System.Drawing.Point(495, 556);
            this.txtProblemasDeSaude.Name = "txtProblemasDeSaude";
            this.txtProblemasDeSaude.Size = new System.Drawing.Size(614, 20);
            this.txtProblemasDeSaude.TabIndex = 235;
            this.txtProblemasDeSaude.TextChanged += new System.EventHandler(this.txtProblemasDeSaude_TextChanged);
            // 
            // txtDiagnostico
            // 
            this.txtDiagnostico.Location = new System.Drawing.Point(495, 522);
            this.txtDiagnostico.Name = "txtDiagnostico";
            this.txtDiagnostico.Size = new System.Drawing.Size(614, 20);
            this.txtDiagnostico.TabIndex = 234;
            this.txtDiagnostico.TextChanged += new System.EventHandler(this.txtDiagnostico_TextChanged);
            // 
            // txtHipoteseDiagnostica
            // 
            this.txtHipoteseDiagnostica.Location = new System.Drawing.Point(495, 487);
            this.txtHipoteseDiagnostica.Name = "txtHipoteseDiagnostica";
            this.txtHipoteseDiagnostica.Size = new System.Drawing.Size(614, 20);
            this.txtHipoteseDiagnostica.TabIndex = 233;
            this.txtHipoteseDiagnostica.TextChanged += new System.EventHandler(this.txtHipoteseDiagnostica_TextChanged);
            // 
            // txtAltura
            // 
            this.txtAltura.Location = new System.Drawing.Point(635, 448);
            this.txtAltura.Name = "txtAltura";
            this.txtAltura.Size = new System.Drawing.Size(50, 20);
            this.txtAltura.TabIndex = 239;
            this.txtAltura.TextChanged += new System.EventHandler(this.txtAltura_TextChanged);
            this.txtAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAltura_KeyPress_1);
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(754, 449);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(54, 20);
            this.txtPeso.TabIndex = 238;
            this.txtPeso.TextChanged += new System.EventHandler(this.txtPeso_TextChanged);
            this.txtPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeso_KeyPress);
            // 
            // txtCodProntuario
            // 
            this.txtCodProntuario.Location = new System.Drawing.Point(495, 449);
            this.txtCodProntuario.Mask = "0000000";
            this.txtCodProntuario.Name = "txtCodProntuario";
            this.txtCodProntuario.Size = new System.Drawing.Size(51, 20);
            this.txtCodProntuario.TabIndex = 241;
            // 
            // txtCodigo1
            // 
            this.txtCodigo1.Location = new System.Drawing.Point(927, 450);
            this.txtCodigo1.Mask = "0000000";
            this.txtCodigo1.Name = "txtCodigo1";
            this.txtCodigo1.Size = new System.Drawing.Size(54, 20);
            this.txtCodigo1.TabIndex = 240;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.Location = new System.Drawing.Point(846, 637);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(291, 60);
            this.panel4.TabIndex = 242;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(846, 581);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(135, 50);
            this.button4.TabIndex = 243;
            this.button4.Text = "Alterar Prontuario";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(359, 671);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 16);
            this.label1.TabIndex = 244;
            this.label1.Text = "(*) CAMPOS OBRIGATÓRIOS !!";
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(886, 181);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(223, 250);
            this.dataGridView2.TabIndex = 247;
            // 
            // txtMedico
            // 
            this.txtMedico.Location = new System.Drawing.Point(886, 155);
            this.txtMedico.Name = "txtMedico";
            this.txtMedico.Size = new System.Drawing.Size(99, 20);
            this.txtMedico.TabIndex = 246;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(991, 150);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(118, 28);
            this.button7.TabIndex = 245;
            this.button7.Text = "Pesquisar Paciente";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // prontuarioMedico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1134, 696);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.txtMedico);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.txtCodProntuario);
            this.Controls.Add(this.txtCodigo1);
            this.Controls.Add(this.txtAltura);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.txtAlergia);
            this.Controls.Add(this.txtMedicamento);
            this.Controls.Add(this.txtProblemasDeSaude);
            this.Controls.Add(this.txtDiagnostico);
            this.Controls.Add(this.txtHipoteseDiagnostica);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtAlergias);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Pai);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtPront);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "prontuarioMedico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "prontuarioMedico";
            this.Load += new System.EventHandler(this.prontuarioMedico_Load);
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btAdm;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btSair1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtPront;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label txtAlergias;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Pai;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAlergia;
        private System.Windows.Forms.TextBox txtMedicamento;
        private System.Windows.Forms.TextBox txtProblemasDeSaude;
        private System.Windows.Forms.TextBox txtDiagnostico;
        private System.Windows.Forms.TextBox txtHipoteseDiagnostica;
        private System.Windows.Forms.TextBox txtAltura;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.MaskedTextBox txtCodProntuario;
        private System.Windows.Forms.MaskedTextBox txtCodigo1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TextBox txtMedico;
        private System.Windows.Forms.Button button7;
    }
}