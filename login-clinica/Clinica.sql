
use master
go
drop database Clinica;
go
create database Clinica;
go
use Clinica;
go
create table perfil_usuario(
cod_perfil int not null  primary key ,
tipo_perfil varchar(30)
);

create table Usuario(
cd_usuario int NOT NULL PRIMARY KEY, 
cd_perfil int,
tipo_perfil varchar(30),
senha_usuario varchar(30),
nome_usuario varchar(30),
FOREIGN KEY (cd_perfil) REFERENCES perfil_usuario(cod_perfil)
);

create table Cadastro_Paciente(
Cod_paciente int primary key not null,
nome varchar(50),
Nascimento datetime,
sexo char,
Tipo_Sanguinio varchar(8),
endereco varchar(50),
bairro varchar(30),
cidade varchar(30),
estado char(2),
cep bigint,
telefone bigint,
celular bigint,
rg bigint,
cpf bigint UNIQUE,
estado_civil varchar(30),
profissao varchar(30),
email varchar(50),
naturalidade varchar(30),
cor varchar(30),
Grau_de_instrucao varchar(40),
conjuge varchar(50),
mae varchar(50),
pai varchar(50),
);


create table Cadastro_Funcionario(
Cod_funcionario int primary key  not null,
nome_funcionario varchar(50) not null,
Nascimento datetime not null,
sexo char,
Tipo_Sanguinio varchar(8),
endereco varchar(50),
bairro varchar(30),
cidade varchar(30),
estado char(2),
cep bigint,
telefone bigint,
celular bigint,
rg bigint,
cpf bigint Unique not null,
estado_civil varchar(30),
email varchar(50),
naturalidade varchar(30),
cor varchar(30),
Grau_de_instrucao varchar(40),
formacao varchar(50),
especialidade varchar(40),
cargo varchar(50),
conjuge varchar(50)
);

create table Cadastro_Medico(
cod_medico int primary key not null,
nome_medico varchar (50) not null,
Nascimento datetime not null,
sexo char,
Tipo_Sanguinio varchar(8),
endereco varchar(50),
bairro varchar(30),
cidade varchar(30),
estado varchar(2),
cep bigint,
telefone bigint,
celular bigint,
rg bigint,
cpf bigint unique,
estado_civil varchar(30),
crm_medico int Unique,
email varchar(50),
naturalidade varchar(30),
cor varchar(30),
especialidade varchar(50),
conjuge varchar(50)

);

create table Agendamento_consulta(
cod_consulta int primary key not null,
Tipo_Agendamento Varchar(30),
Tipo_Consulta varchar(30),
Data_Consulta DateTime,
DataAte datetime,
Medico int,
Cod_pacient int,
foreign key (Medico) references Cadastro_Medico (cod_medico),
foreign key (Cod_pacient) references Cadastro_Paciente (Cod_paciente),
Status_Consulta varchar(16),
Data_Agendamento DateTime,
Data varchar(18)
);

create table Historico_Agendamento(
cod_historico int primary key not null,
Tipo_Agendamento Varchar(30),
Tipo_Consulta varchar(30),
Data_Consulta DateTime,
DataAte datetime,
Medico int,
Cod_pacient int,
foreign key (Medico) references Cadastro_Medico (cod_medico),
foreign key (Cod_pacient) references Cadastro_Paciente (Cod_paciente),
Status_Consulta varchar(16),
Data_Agendamento DateTime,
Data varchar(18)
);

create table Cadastro_Relatorio(
cod_relatorio int primary key identity,
tipo_relatorio varchar(25),
data_Relatorio datetime,
Codigo_Paciente int,
Codigo_medico int,
relatorio varchar(MAX),
foreign key (Codigo_Paciente) references Cadastro_Paciente (Cod_paciente),
foreign key (Codigo_medico) references Cadastro_Medico (cod_medico)
);

create table prontuario(
cod_prontuario int primary key identity,
Cod_pacient int ,
FOREIGN KEY (Cod_pacient) REFERENCES Cadastro_Paciente (Cod_paciente),
altura float,
peso float,
medicamento varchar(150),
diaginosticos varchar(150),
alergias varchar(150),
hipotese_diagnostica varchar(150),
problemas_Saude varchar(150),
);


create table ImagemPaciente(
nome varchar(30),
foto varbinary(max)
);

create table ImagemMedico(
nome varchar(30),
foto varbinary(max)
);

create table ImagemFuncionario(
nome varchar(30),
foto varbinary(max)
);


insert into perfil_usuario values (1,'Adiministrador');
insert into perfil_usuario values (2,'Funcionario');
insert into perfil_usuario values (3,'M�dico');

insert into  Usuario values (1,2,'Funcion�rio','3216','fernando');
insert into  Usuario values (2,1,'Administrador','1234','felipe');
insert into  Usuario values (3,3,'M�dico','123','Ramada');


SELECT * FROM perfil_usuario;